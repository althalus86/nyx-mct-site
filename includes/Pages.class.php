<?php

class PageType{
	
	public $ID = 0;
	public $InternalName = "";
	public $Title = "";
	
	function __construct($Type) 
	{
		$this->ID = $Type->id;
		$this->InternalName = $Type->internal_name;
		$this->Title = $Type->title;
	}
}

class BasicPage { 

	public $Title = "";
	public $TypeID = "";
	public $RootID = "";
	public $ParentID = "";
	public $Status = "";
	public $StatusReadable = "";
	public $RevisionNumber = "";
	public $TimeCreated = "";
	public $TimeLastUpdated = "";
	public $OwnerID = "";
	public $ModifiedByID = "";
	public $Order = "";
	public $Sortable = "";
	public $FullPage = "";
	public $Root = "";
	public $PageID = "";
	
	private  $Type = "";
	
	function __construct($dbPage, $isBasic = true) 
	{
		
		global $toRoot;
		if (!isset($toRoot)){
			$toRoot = "";
		}
		$this->Root = $toRoot;
		
		
		$this->FullPage = $dbPage;
		$this->PageID = $dbPage->page_id;
		$this->Title = $dbPage->title;
		$this->TypeID = $dbPage->type;
		$this->RootID = $dbPage->root_id;
		$this->ParentID = $dbPage->parent_id;
		$this->Status = $dbPage->state;
		$this->RevisionNumber = $dbPage->revision_number;
		$this->TimeCreated = $dbPage->time_created;
		$this->TimeLastUpdated = $dbPage->time_last_updated;
		$this->OwnerID = $dbPage->owner_id;
		$this->Order = $dbPage->order;
		$this->ModifiedByID = $dbPage->modified_by_id;
		if($isBasic){
			$this->Sortable = $dbPage->enable_sorting;
		}

		$this->setStatusReadable();
	}
	
	function setStatusReadable(){
		switch ($this->Status){
			case 0:
				$this->StatusReadable = "Draft";
				break;
			case 1:
				$this->StatusReadable = "Published";
				break;
		}
	}
	
	function getPageType(){
		if (is_object($this->Type)){
			if($this->Type->ID == $this->TypeID){
				return $this->Type;
			}
		}
		
		include_once($this->Root."includes/SQL.class.php");
		$db = new SQL();
		$this->Type = new PageType($db->pageGetType($this->TypeID));
		return $this->Type;
	}

}

class Page extends BasicPage { 
	
	public $MetaData = "";
	public $KeywordsIdCSV = "";
	public $PermaLink = "";
	public $LanguageID = "";
	public $LanguagePageID = "";
	
	function __construct($dbPage) 
	{
		parent::__construct($dbPage, false);
		
		$this->MetaData = $dbPage->metadata;
		$this->KeywordsIdCSV = $dbPage->keywords;
		$this->PermaLink = $dbPage->permalink;
		$this->LanguageID = $dbPage->language_id;
		$this->LanguagePageID = $dbPage->language_page_id;
	}
}


class PageStatic extends Page { 

	public $ID = "";
	public $Content = "";
	
	
	function __construct($dbPage) 
	{
		parent::__construct($dbPage);
		$this->Content = $dbPage->content;
		$this->ID = $dbPage->id;

	}
}

class PageMediaList extends Page { 

	public $ID = "";
	public $Content = "";
	public $OrderBy = 0;
	
	
	function __construct($dbPage) 
	{
		parent::__construct($dbPage);
		$this->Content = $dbPage->content;
		$this->ID = $dbPage->id;
		$this->OrderBy = $dbPage->order_by;
	}
}


class PageLink extends Page { 

	public $ID = "";
	public $Link = "";
	public $External = "";
	
	
	function __construct($dbPage) 
	{
		parent::__construct($dbPage);
		$this->ID = $dbPage->id;
		$this->Link = $dbPage->link;
		$this->External = $dbPage->external;
		

	}
}

?>