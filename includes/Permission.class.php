<?php

class Permission { 

	public $UserID = "";
	public $Root = "";
	protected $DefaultPermission = "";
	
	function __construct($dbPermission, $userID, $isBasic = true, $defaultPermission = 1) 
	{
		
		global $toRoot;
		if (!isset($toRoot)){
			$toRoot = "";
		}
		$this->Root = $toRoot;
		
		$this->UserID = $userID;
		$this->DefaultPermission = $defaultPermission;
	}
	

}


class PagePermission extends Permission { 

	public $PageID = "";
	public $Add = "";
	public $Delete = "";
	public $Edit = "";
	public $AddLink = "";
	public $Sortable = "";
	
	
	function __construct($dbPermission, $userID) 
	{
		parent::__construct($dbPermission, $userID, false);
		
		if(isset($dbPermission->page_id)){ $this->PageID = $dbPermission->page_id; } else {$this->PageID = $this->DefaultPermission;}
		if(isset($dbPermission->add)){ $this->Add = $dbPermission->add;  } else {$this->Add = $this->DefaultPermission;}
		if(isset($dbPermission->delete)){ $this->Delete = $dbPermission->delete; } else {$this->Delete = $this->DefaultPermission;}
		if(isset($dbPermission->edit)){ $this->Edit = $dbPermission->edit; } else {$this->Edit = $this->DefaultPermission;}
		if(isset($dbPermission->add_link)){ $this->AddLink = $dbPermission->add_link; } else {$this->AddLink = $this->DefaultPermission;}
		if(isset($dbPermission->sortable)){ $this->Sortable = $dbPermission->sortable; } else {$this->Sortable = $this->DefaultPermission;}

	}
}


?>