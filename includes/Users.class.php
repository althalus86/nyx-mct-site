<?php 

class User { 

	var $Username = "";
	var $Name = "";
	var $Surname = "";
	var $Email = "";
	var $IsAdmin = false;
	var $ID = 0;
	
	function __construct($dbUser) 
	{
		$this->Username = $dbUser->username;
		$this->Name = $dbUser->name;
		$this->Surname = $dbUser->surname;
		$this->Email = $dbUser->email;
		$this->IsAdmin = $dbUser->is_admin;
		$this->ID = $dbUser->id;
	}

}



?>