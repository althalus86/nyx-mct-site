<?php
if (!isset($toRoot)){
	$toRoot = "";
}
include_once($toRoot."includes/Users.class.php");
include_once($toRoot."includes/Pages.class.php");
include_once($toRoot."includes/Security.class.php");

class SQL extends MySQLi{ 
	
	
	function __construct() 
	{ 
		global $db_host, $db_name, $db_username, $db_password, $db_port;
		parent::__construct($db_host, $db_username, $db_password, $db_name, $db_port);
		
		 if (mysqli_connect_errno())
		{
			die(printf('MySQL Server connection failed: %s', mysqli_connect_error()));
		}
		
	}
	
	/**** Sessions  ****/
	function sessionCreate($username, $password){
		$salt = substr(md5(time()),10,15);
		if($this->multi_query( "CALL session_create('".$username."', '".md5(md5($password).$salt)."', '".$salt."', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."')" )){
			$repeat = true;
			do {
				if ($result = $this->store_result()) {
					while ($row = $result->fetch_row()) {
						if(isset($session)){
							$user = $row[0];
						}else{
							$session = $row[0];
						}
					}
					$result->free();
				}
				if ($this->more_results()) {
					$this->next_result();
				}else{
					$repeat = false;
				}
			} while ($repeat);
			$this->next_result();
		}
		
		$security = new Security();
		$security->login($session, $user);
		return $security;
	}
	
	function sessionCheck($session){
		$rs = $this->query( "CALL session_check('".$session."', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->result;
	}
	
	
	/**** Pages  ****/
	
	function pageCreate($permaLink, $languageID, $title, $parentID, $userID, $typeID, $state){
		$rs = $this->query( "CALL page_create('".$permaLink."', ". $languageID.", '". $title."', ". $parentID.", ". $userID.", ". $typeID.", ". $state.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->page_language_id;
	}
	
	function pageSaveOrder($pageID, $order){
		$rs = $this->query( "CALL page_save_order(".$pageID.", ". $order.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	
	function pageGetParentID($pageID){
		if($pageID > 0){
			$rs = $this->query( "CALL page_get_parent_id(".$pageID.")" );
			$row = $rs->fetch_object();
			$this->next_result();
			return $row->parent_id;
		}
		return 0;
	}
	
	
	function pageGet($permaLink, $languageCode){
		$rs = $this->query( "CALL page_get('".$permaLink."', '". $languageCode."')" );
		if($rs == false){
			return false;
		}else{
			$row = $rs->fetch_object();
			$this->next_result();
			$myPage = new Page($row);
			return $myPage;
		}
	}
	
	function pageGetByLanguageID($id){
		$rs = $this->query( "CALL page_get_by_language_id(".$id.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		$myPage = new Page($row);
		return $myPage;
	}
	
	function pageGetByPageIDandLanguageCode($pageID, $languageCode){
	//	echo "CALL page_get_by_page_id_and_language_code(".$pageID.", '".$languageCode."')";
		$rs = $this->query( "CALL page_get_by_page_id_and_language_code(".$pageID.", '".$languageCode."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		$myPage = new Page($row);
		return $myPage;
	}
	
	function pageGetListAtLevel($level){
		$rs = $this->query( "CALL page_get_list_at_level(".$level.")" );
		$pageList = array();
		while($row = $rs->fetch_object()){
			$basicPage = new BasicPage($row);
			array_push($pageList, $basicPage);
		}
		$this->next_result();
		return $pageList;
	}
	
	function pageGetAllTypes(){
		$rs = $this->query( "CALL page_get_all_types()" );
		$pageTypeList = array();
		while($row = $rs->fetch_object()){
			array_push($pageTypeList, $row);
		}
		$this->next_result();
		return $pageTypeList;
	}
	
	function pageGetSubmenuLinks($PageID, $langaugeID){
		$rs = $this->query( "CALL page_get_submenu_links_by_page_id(".$PageID.", ".$langaugeID.")" );
		$pageSubmenulinks = array();
		while($row = $rs->fetch_object()){
			array_push($pageSubmenulinks, $row);
		}
		$this->next_result();
		return $pageSubmenulinks;
	}
	
	
	
	function pageGetType($typeID){
		$rs = $this->query( "CALL page_get_type(".$typeID.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	
	function pageGetNextPermalink($permalink){
		$rs = $this->query( "CALL page_get_next_permalink('".$permalink."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->newPermaLink;
	}
	
	
	
	function pageDeleteByPageID($PageID,  $iUserID){
		$rs = $this->query( "CALL page_delete_by_page_id(".$PageID.", ".$iUserID.")" );
		$pageChildrenIDs = array();
		while($row = $rs->fetch_assoc()){
			array_push($pageChildrenIDs, $row['page_id']);
		}
		$this->next_result();
		$childPages = count($pageChildrenIDs);
		foreach($pageChildrenIDs as $pageChildID){
			$childPages += $this->pageDeleteByPageID($pageChildID, $iUserID);
		}
		return $childPages;
	}
	
	/****** PageTypes ******/
	
	function pageCreateStatic( $languagePageID, $content){
		$rs = $this->query( "CALL page_create_static(".$languagePageID.", '". $content."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageUpdateStatic($languagePageID, $iContent, $iUserID, $iTitle){
		$rs = $this->query( "CALL page_update_static(".$languagePageID.", '".$iContent."', ".$iUserID.", '".$iTitle."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageCreateLink( $languagePageID, $link, $external=0){
		$rs = $this->query( "CALL page_create_link(".$languagePageID.", '". $link."', ".$external.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageUpdateLink($languagePageID, $link, $external, $iUserID, $iTitle, $iPermaLink){
		$rs = $this->query( "CALL page_update_link(".$languagePageID.", '".$link."', ".$external.", ".$iUserID.", '".$iTitle."', '".$iPermaLink."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	
	/**** Users  ****/
	function getUserByID($user_id){
		$rs = $this->query( "CALL get_user_by_id(".$user_id.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		$user = new User($row);
		return $user;
	}
	
	function userGetCurrent(){
		if(!isset($_SESSION)){
			session_start();
		}
		$rs = $this->query( "CALL user_get_current('".$_SESSION['session']."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		$user = new User($row);
		return $user;
	}

}


?>