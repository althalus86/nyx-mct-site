<?php

class StatusMsg{

	var $statusID = -1;	
	var $statusMsg = "Status Message Not Set";
	var $statusClass = "info";
	
	function setStatusID($statusID){
		$this->statusID = $statusID;
		switch($statusID){
			case 100:
				$this->statusMsg = "Username doesn't exist";
				$this->statusClass = "error";
				break;
			case 107:
				$this->statusMsg = "Invalid username and/or password";
				$this->statusClass = "error";
				break;
			case 203:
				$this->statusMsg = "Salt already used";
				$this->statusClass = "error";
				break;
			case 204:
				$this->statusMsg = "Session expired. Please login again";
				$this->statusClass = "error";
				break;
			default:
				$this->statusMsg = "Unknown status";
				$this->statusClass = "info";
				break;
			
		}
	}
	
	function setCustomStatus($message, $statusClass, $statusId = 0){
		$this->statusMsg = $message;
		$this->statusClass = $statusClass;
		$this->statusID = $statusId;
	}
	
}


?>