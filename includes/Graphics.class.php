<?php
/*

Sample Code -- 
	$image = new resizeImage;
	$image->setImage(path to image to resize);
	$image->createThumb(150,100); //if 0 is given as any one of the lengths the image would be resized keep the full original content keeping constant ratio
	$image->saveThumb("default.jpg", $album_folder );
	$image->renderThumb();

*/
class resizeImage{
	var $imgSrc,$myImage,$cropHeight,$cropWidth,$x,$y,$thumb, $imgWidth, $imgHeight;  
	
	function setImage($image)
	{
	
	//Your Image
	   $this->imgSrc = $image; 
	                     
	//getting the image dimensions
	   list($width, $height) = getimagesize($this->imgSrc); 
	   $this->imgWidth = $width;
	   $this->imgHeight = $height;
	                     
	//create image from the jpeg
	   $this->myImage = imagecreatefromjpeg($this->imgSrc) or die("Error: Cannot find image!"); 
	            
	       if($width > $height) $biggestSide = $width; //find biggest length
	       else $biggestSide = $height; 
	                     
	//The crop size will be half that of the largest side 
	   $cropPercent = .5; // This will zoom in to 50% zoom (crop)
	   $this->cropWidth   = $biggestSide*$cropPercent; 
	   $this->cropHeight  = $biggestSide*$cropPercent; 
	                     
	                     
	//getting the top left coordinate
	   $this->x = ($width-$this->cropWidth)/2;
	   $this->y = ($height-$this->cropHeight)/2;
	             
	}

	
	function createThumb($width, $height, $mode="none")
	{
		if(($height == 0) || ($width == 0)){
			if($height ==0){
				$height = ($width*$this->imgHeight)/$this->imgWidth;
			}else{
				$width = ($height*$this->imgWidth)/$this->imgHeight;
			}
			$this->x = 0;
			$this->y = 0;
			$this->cropWidth = $this->imgWidth;
			$this->cropHeight = $this->imgHeight;
			
		}else{
			$horzRatio = $this->imgWidth/$width;
			$vertRatio = $this->imgHeight/$height;
			
			if($horzRatio < $vertRatio){
				
				$this->x = 0;
				$this->cropWidth = $this->imgWidth;
				$this->cropHeight = (($height*$this->imgWidth)/$width);
				$this->y = ($this->imgHeight-$this->cropHeight)/2;
			}else{
				$this->y = 0;
				$this->cropHeight = $this->imgHeight;
				$this->cropWidth = (($width*$this->imgHeight)/$height);
				$this->x = ($this->imgWidth-$this->cropWidth)/2;
			}
		}
		
		if($mode == "fitin"){
			$horzRatio = $this->imgWidth/$width;
			$vertRatio = $this->imgHeight/$height;
			
			if($horzRatio < $vertRatio){
				$width = (($height*$this->imgWidth)/$this->imgHeight);
			}else{
				$height = (($width*$this->imgHeight)/$this->imgWidth);
			}
			$mode = "nocrop";
		}
	                    
	  $this->thumb = imagecreatetruecolor($width, $height); 
	  if($mode != "nocrop"){
		  
	  		imagecopyresampled($this->thumb, $this->myImage, 0, 0,$this->x, $this->y, $width, $height, $this->cropWidth, $this->cropHeight); 
	  }else{
			imagecopyresampled($this->thumb, $this->myImage, 0, 0,0, 0, $width, $height, $this->imgWidth, $this->imgHeight); 
	  }
	}  
	
	
	function saveThumb($imgname, $dpath)
	{
	                     
	  // header('Content-type: image/jpeg');
	  
	  ImageJPEG ($this->thumb, $dpath.$imgname, 100);
	  // imagejpeg($this->thumb);
	  // imagedestroy($this->thumb); 
	}  
	
	function renderThumb()
	{
	                     
	  header('Content-type: image/jpeg');
	  ImageJPEG ($this->thumb, NULL, 100);
	  // imagejpeg($this->thumb);
	  // imagedestroy($this->thumb); 
	}  
	
	function renderImage(){
		header('Content-type: image/jpeg');
	  	ImageJPEG ($this->myImage, NULL, 100);
	}
	
	
}  
 
?>