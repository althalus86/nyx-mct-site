<?php 

//include_once("../includes/SQL.class.php");

class Security{
	
	var $UserID = 0;
	var $StatusMsg = "";
	var $SessionString = "";
	var $LoggedIn = false;
	var $Root = "";
	public $DB = "";
	
	function __construct() 
	{ 
		global $toRoot, $isLogin, $cms_login_path;
		if (!isset($toRoot)){
			$toRoot = "";
		}
		$this->Root = $toRoot;
		
		include_once($this->Root."includes/SQL.class.php");
		$this->DB = new SQL();
		
		if(!isset($isLogin)){
			session_start();
			$errorNumber = 205;
			if(isset($_SESSION['session'])){
				
				if(($result = $this->DB->sessionCheck($_SESSION['session'])) >0){
					$this->StatusMsg = new StatusMsg();
					$this->UserID = $result;
					$this->StatusMsg->setStatusID(0);
					$this->SessionString = $_SESSION['session'];
					$this->LoggedIn = true;
				}else{
					$errorNumber = $result*(-1);
				}
			}
			if(!$this->LoggedIn){
				header( 'Location: http://'.$_SERVER['HTTP_HOST'].$cms_login_path.'?p='.$_SERVER['PHP_SELF'].'&e='.$errorNumber ) ;
				//include($this->Root.'cms/login.php');
				//exit();
			}
		}
		
	}
	
	function login($sessionResult, $userID){
		
		include_once($this->Root."includes/StatusMsg.class.php");
		
		$this->StatusMsg = new StatusMsg();
		
		if(strlen($sessionResult)< 7){
			$this->StatusMsg->setStatusID($sessionResult);
		}else{
			$this->UserID = $userID;
			$this->StatusMsg->setStatusID(0);
			$this->SessionString = $sessionResult;
			$this->LoggedIn = true;
			session_start();
			$_SESSION['session'] = $sessionResult;
		}
	}
	
	
	
	

}




?>