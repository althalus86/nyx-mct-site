<?php

include_once($toRoot."includes/Graphics.class.php");

class MediaType{
	
	public $ID = 0;
	public $InternalName = "";
	public $Title = "";
	
	function __construct($Type) 
	{
		$this->ID = $Type->id;
		$this->InternalName = $Type->internal_name;
		$this->Title = $Type->title;
	}
}

class BasicMedia { 
	
	public $ID = "";
	public $Title = "";
	public $TypeID = "";
	public $RootID = "";
	public $ParentID = "";
	public $Status = "";
	public $StatusReadable = "";
	public $RevisionNumber = "";
	public $TimeCreated = "";
	public $TimeLastUpdated = "";
	public $OwnerID = "";
	public $ModifiedByID = "";
	public $Order = "";
	public $Extension = "";
	public $FullPage = "";
	public $Root = "";
	public $Path = "";
	public $Filename = "";
	public $FullPath = "";
	private  $Type = "";
	
	function __construct($dbPage, $isBasic = true) 
	{
		
		global $toRoot;
		if (!isset($toRoot)){
			$toRoot = "";
		}
		$this->Root = $toRoot;
		
		
		$this->FullPage = $dbPage;
		$this->ID = $dbPage->id;
		$this->Title = $dbPage->title;
		$this->TypeID = $dbPage->type;
		$this->RootID = $dbPage->root_id;
		$this->ParentID = $dbPage->parent_id;
		$this->Status = $dbPage->state;
		$this->RevisionNumber = $dbPage->revision_number;
		$this->TimeCreated = $dbPage->time_created;
		$this->TimeLastUpdated = $dbPage->time_last_updated;
		$this->OwnerID = $dbPage->owner_id;
		$this->Order = $dbPage->order;
		$this->ModifiedByID = $dbPage->modified_by_id;
		$this->Extension = $dbPage->extension;
		$this->Path = $dbPage->path;
		$this->Filename = $dbPage->filename;
		$this->FullPath = $this->setFullPath();
		$this->setStatusReadable();
	}
	
	function setStatusReadable(){
		switch ($this->Status){
			case 0:
				$this->StatusReadable = "Draft";
				break;
			case 1:
				$this->StatusReadable = "Published";
				break;
		}
	}
	
	function setFullPath(){
		return  $this->Path."/".$this->Filename;
	}
	
	function getMediaType(){
		if (is_object($this->Type)){
			if($this->Type->ID == $this->TypeID){
				return $this->Type;
			}
		}
		
		include_once($this->Root."includes/SQL.class.php");
		$db = new SQL();
		$this->Type = new MediaType($db->mediaGetType($this->TypeID));
		return $this->Type;
	}
	
	function getThumbURL($forceCreate = true){
		if ($forceCreate){
			if (!file_exists("media/thumbs/".$this->FullPath)){
				//return "media/2thumbs".$this->FullPath;
				$image = new resizeImage;
				$image->setImage("media/original/".$this->FullPath);
				$image->createThumb(140,140, "fitin"); //if 0 is given as any one of the lengths the image would be resized keep the full original content keeping constant ratio
				if (!file_exists("media/thumbs/".$this->Path))
					@mkdir("media/thumbs/".$this->Path, 0777, true);
				$image->saveThumb($this->Filename, "media/thumbs/".$this->Path."/" );
			//$image->renderThumb();
			}
		}
		return "media/thumbs/".$this->FullPath;
	}
	
	function getCustomURL($name, $width, $height, $root = "", $mode = "none", $forceCreate = false){
		
		if ((!file_exists($root."media/".$name."/".$this->FullPath) || $forceCreate)){
			//return "media/2thumbs".$this->FullPath;
			$image = new resizeImage;
			$image->setImage($root."media/original/".$this->FullPath);
			$image->createThumb($width,$height, $mode); //if 0 is given as any one of the lengths the image would be resized keep the full original content keeping constant ratio
			if (!file_exists($root."media/".$name."/".$this->Path))
				@mkdir($root."media/".$name."/".$this->Path, 0777, true);
			$image->saveThumb($this->Filename, $root."media/".$name."/".$this->Path."/" );
		//$image->renderThumb();
		}
		
		return $root."media/".$name."/".$this->FullPath;
	}
	
	function getFileTypeIconURL($root = ""){
	//echo "../cms/res/images/types/".substr($this->Extension, 1).".png";
		if (file_exists($root."res/images/types/".substr($this->Extension, 1).".png")){
			echo $root."res/images/types/".substr($this->Extension, 1).".png";
		}else{
			echo $root."res/images/types/unknown.png";
		}
	}

}



class Media extends BasicMedia { 
	
	public $MetaData = "";
	public $KeywordsIdCSV = "";
	public $PermaLink = "";
	public $LanguageID = "";
	public $LanguagePageID = "";
	
	function __construct($dbMedia) 
	{
		parent::__construct($dbMedia, false);
		
		/*$this->MetaData = $dbPage->metadata;
		$this->KeywordsIdCSV = $dbPage->keywords;
		$this->PermaLink = $dbPage->permalink;
		$this->LanguageID = $dbPage->language_id;
		$this->LanguagePageID = $dbPage->language_page_id;*/
	}
}

class MediaDocs extends Media { 
	
	public $Author = "";
	public $DateOfPublication = "";
	public $LanguageID = "";
	public $Size = "";
	
	function __construct($dbMedia) 
	{
		parent::__construct($dbMedia);
		
		$this->Author = $dbMedia->author;
		$this->DateOfPublication = $dbMedia->date_of_publication;
		$this->LanguageID = $dbMedia->language_id;
		$this->Size = $dbMedia->size;
	}
}

/*

class MediaStatic extends Media { 

	public $ID = "";
	public $Content = "";
	
	
	function __construct($dbPage) 
	{
		parent::__construct($dbPage);
		$this->Content = $dbPage->content;
		$this->ID = $dbPage->id;

	}
}


class MediaLink extends Media { 

	public $ID = "";
	public $Link = "";
	public $External = "";
	
	
	function __construct($dbPage) 
	{
		parent::__construct($dbPage);
		$this->ID = $dbPage->id;
		$this->Link = $dbPage->link;
		$this->External = $dbPage->external;
		

	}
}
*/
?>