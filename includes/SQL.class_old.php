<?php
if (!isset($toRoot)){
	$toRoot = "";
}
include_once($toRoot."includes/Users.class.php");
include_once($toRoot."includes/Pages.class.php");
include_once($toRoot."includes/Permission.class.php");
include_once($toRoot."includes/Security.class.php");

class SQL extends MySQLi{ 
	
	
	function __construct() 
	{ 
		global $db_host, $db_name, $db_username, $db_password, $db_port;
		parent::__construct($db_host, $db_username, $db_password, $db_name, $db_port);
		
		 if (mysqli_connect_errno())
		{
			die(printf('MySQL Server connection failed: %s', mysqli_connect_error()));
		}
		
	}
	
	/**** Sessions  ****/
	function sessionCreate($username, $password){
		$salt = substr(md5(time()),10,15);
		if($this->multi_query( "CALL session_create('".$username."', '".md5(md5($password).$salt)."', '".$salt."', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."')" )){
			$repeat = true;
			do {
				if ($result = $this->store_result()) {
					while ($row = $result->fetch_row()) {
						if(isset($session)){
							$user = $row[0];
						}else{
							$session = $row[0];
						}
					}
					$result->free();
				}
				if ($this->more_results()) {
					$this->next_result();
				}else{
					$repeat = false;
				}
			} while ($repeat);
			if ($this->more_results()) {
				$this->next_result();
			}
		}
		
		$security = new Security();
		$security->login($session, $user);
		return $security;
	}
	
	function sessionCheck($session){
		$rs = $this->query( "CALL session_check('".$session."', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->result;
	}
	
	
	/**** Languages  ****/
	function languagesGetAll(){
		$rs = $this->query( "CALL languages_get_all()" );
		$languageList = array();
		while($row = $rs->fetch_object()){
			array_push($languageList, $row);
		}
		$this->next_result();
		return $languageList;
	}
	
	
	/**** Permissions  ****/
	
	function permissionsPageGetByPageID($pageID, $userID){
		$query =  "CALL permissions_pages_get_by_page_id(".$pageID.", ". $userID.")";
	//	echo $query;
		$rs = $this->query( $query );
		
			$row = $rs->fetch_object();
			$this->next_result();
			$permission = new PagePermission($row, $userID);
			return $permission;
		
	}
	
	
	/**** Pages  ****/
	
	function pageCreate($permaLink, $languageID, $title, $parentID, $userID, $typeID, $state){
		$rs = $this->query( "CALL page_create('".$permaLink."', ". $languageID.", '". $title."', ". $parentID.", ". $userID.", ". $typeID.", ". $state.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->page_language_id;
	}
	
	function pageSaveOrder($pageID, $order){
		$rs = $this->query( "CALL page_save_order(".$pageID.", ". $order.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	
	function pageGetParentID($pageID){
		if($pageID > 0){
			$rs = $this->query( "CALL page_get_parent_id(".$pageID.")" );
			$row = $rs->fetch_object();
			$this->next_result();
			return $row->parent_id;
		}
		return 0;
	}
	
	
	function pageGet($permaLink, $languageCode){
		$rs = $this->query( "CALL page_get('".$permaLink."', '". $languageCode."')" );
		if($rs == false){
			return false;
		}else{
			$row = $rs->fetch_object();
			$this->next_result();
			$myPage = new Page($row);
			return $myPage;
		}
	}
	
	function pageGetByLanguageID($id){
		//echo "CALL page_get_by_language_id(".$id.")" ;
		$rs = $this->query( "CALL page_get_by_language_id(".$id.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		$myPage = new Page($row);
		return $myPage;
	}
	
	function pageGetByPageIDandLanguageCode($pageID, $languageCode){
	//	echo "CALL page_get_by_page_id_and_language_code(".$pageID.", '".$languageCode."')";
		$rs = $this->query( "CALL page_get_by_page_id_and_language_code(".$pageID.", '".$languageCode."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		$myPage = new Page($row);
		return $myPage;
	}
	
	function pageGetListAtLevel($level){
		$rs = $this->query( "CALL page_get_list_at_level(".$level.")" );
		$pageList = array();
		while($row = $rs->fetch_object()){
			$basicPage = new BasicPage($row);
			array_push($pageList, $basicPage);
		}
		$this->next_result();
		return $pageList;
	}
	
	function pageGetMediaIDList($pageID, $groupID = 0){
		$rs = $this->query( "CALL page_get_media_id_list_by_group(".$pageID.", ".$groupID.")" );
		$mediaIDList = array();
		while($row = $rs->fetch_object()){
			//$basicPage = new BasicPage($row);
			array_push($mediaIDList, $row->media_id);
		}
		$this->next_result();
		return $mediaIDList;
	}
	
	
	
	function pageGetAllTypes(){
		$rs = $this->query( "CALL page_get_all_types()" );
		$pageTypeList = array();
		while($row = $rs->fetch_object()){
			array_push($pageTypeList, $row);
		}
		$this->next_result();
		return $pageTypeList;
	}
	
	function pageGetSubmenuLinks($PageID, $langaugeID){
		$rs = $this->query( "CALL page_get_submenu_links_by_page_id(".$PageID.", ".$langaugeID.")" );
		$pageSubmenulinks = array();
		while($row = $rs->fetch_object()){
			array_push($pageSubmenulinks, $row);
		}
		$this->next_result();
		return $pageSubmenulinks;
	}
	
	function pageGetAvailablePermalinks(){
		$rs = $this->query( "CALL page_get_available_permalinks()" );
		$pagePermaLinks = array();
		while($row = $rs->fetch_object()){
			array_push($pagePermaLinks, $row);
		}
		$this->next_result();
		return $pagePermaLinks;
	}
	
	
	
	function pageGetType($typeID){
		$rs = $this->query( "CALL page_get_type(".$typeID.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	
	function pageGetNextPermalink($permalink){
		$rs = $this->query( "CALL page_get_next_permalink('".$permalink."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->newPermaLink;
	}
	
	
	
	function pageDeleteByPageID($PageID,  $iUserID){
		$rs = $this->query( "CALL page_delete_by_page_id(".$PageID.", ".$iUserID.")" );
		$pageChildrenIDs = array();
		while($row = $rs->fetch_assoc()){
			array_push($pageChildrenIDs, $row['page_id']);
		}
		$this->next_result();
		$childPages = count($pageChildrenIDs);
		foreach($pageChildrenIDs as $pageChildID){
			$childPages += $this->pageDeleteByPageID($pageChildID, $iUserID);
		}
		return $childPages;
	}
	
	function pageAddMedia($pageID, $mediaID, $userID){
		$rs = $this->query( "CALL page_add_media(".$pageID.", ".$mediaID.", ".$userID.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageAddMediaInGroup($pageID, $mediaID, $groupID, $userID){
		$query =  "CALL page_add_media_in_group(".$pageID.", ".$mediaID.", ".$groupID.", ".$userID.")";
		//echo $query;
		$rs = $this->query( $query);
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageDeleteMedia($pageID, $mediaID, $userID){
		$rs = $this->query( "CALL page_delete_media(".$pageID.", ".$mediaID.", ".$userID.")" );
		//$row = $rs->fetch_object();
	//	$this->next_result();
		return true;
	}
	
	/****** PageTypes ******/
	
	function pageCreateStatic( $languagePageID, $content){
		$rs = $this->query( "CALL page_create_static(".$languagePageID.", '". $content."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageUpdateStatic($languagePageID, $iContent, $iUserID, $iTitle){
		$rs = $this->query( "CALL page_update_static(".$languagePageID.", '".$iContent."', ".$iUserID.", '".$iTitle."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageCreateNewsPost( $languagePageID, $content){
		$rs = $this->query( "CALL page_create_news_post(".$languagePageID.", '". $content."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageUpdateNewsPost($languagePageID, $iContent, $iUserID, $iTitle){
		$rs = $this->query( "CALL page_update_news_post(".$languagePageID.", '".$iContent."', ".$iUserID.", '".$iTitle."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageCreateLink( $languagePageID, $link, $external=0){
		$rs = $this->query( "CALL page_create_link(".$languagePageID.", '". $link."', ".$external.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageUpdateLink($languagePageID, $link, $external, $iUserID, $iTitle, $iPermaLink){
		$rs = $this->query( "CALL page_update_link(".$languagePageID.", '".$link."', ".$external.", ".$iUserID.", '".$iTitle."', '".$iPermaLink."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageCreateMediaList( $languagePageID, $content){
		$rs = $this->query( "CALL page_create_media_list(".$languagePageID.", '". $content."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function pageUpdateMediaList($languagePageID, $iContent, $iUserID, $iTitle, $iOrderBy){
		$rs = $this->query( "CALL page_update_media_list(".$languagePageID.", '".$iContent."', ".$iUserID.", '".$iTitle."', ".$iOrderBy.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	
	
	
	
	/**** Media  ****/
	
	function mediaCreate( $title, $parentID, $userID, $typeID, $state){
		$rs = $this->query( "CALL media_create('". $title."', ". $parentID.", ". $userID.", ". $typeID.", ". $state.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->media_id;
	}
	
	function mediaCreateForUpload( $title, $parentID, $userID, $typeID, $state, $path, $extension, $filename){
		$query =  "CALL media_create_for_upload('". $title."', ". $parentID.", ". $userID.", ". $typeID.", ". $state.", '". $path."', '". $extension."', '". $filename."')";
		//echo $query;
		$rs = $this->query( $query );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row->media_id;
	}
	
	function mediaGetListAtLevel($level){
		$rs = $this->query( "CALL media_get_list_at_level(".$level.")" );
		$mediaList = array();
		while($row = $rs->fetch_object()){
			$basicMedia = new BasicMedia($row);
			array_push($mediaList, $basicMedia);
		}
		$this->next_result();
		return $mediaList;
	}
	
	function mediaGetByID($ID){
		$rs = $this->query( "CALL media_get_by_id(".$ID.")" );
		//echo "hello";
		if($this->more_results()){
			$row = $rs->fetch_object();
			$this->next_result();
			return new BasicMedia($row);
		}else{
			return NULL;
		}
		
		 /*
		$mediaList = array();
		while($row = $rs->fetch_object()){
			$basicMedia = new BasicMedia($row);
			array_push($mediaList, $basicMedia);
		}
		$this->next_result();
		return $mediaList; */
	}
	
	function mediaGetInfoDocsByID($ID){
		$query = "CALL media_get_info_docs_by_id(".$ID.")";
		//echo $query;
		$rs = $this->query( $query );
		$row = $rs->fetch_object();
		$this->next_result();
		return new MediaDocs($row); /*
		$mediaList = array();
		while($row = $rs->fetch_object()){
			$basicMedia = new BasicMedia($row);
			array_push($mediaList, $basicMedia);
		}
		$this->next_result();
		return $mediaList; */
	}
	
	function mediaUpdateInfoDocsByID($mediaID, $title, $author, $languageID, $DOP, $userID){
		$query = "CALL media_update_info_docs_by_id(".$mediaID.", '". $title."', '". $author."', ". $languageID.", '".$DOP."', ". $userID.")";
		//echo $query;
		$rs = $this->query( $query );
		$row = $rs->fetch_object();
		$this->next_result();
		return true;
		/*
		$mediaList = array();
		while($row = $rs->fetch_object()){
			$basicMedia = new BasicMedia($row);
			array_push($mediaList, $basicMedia);
		}
		$this->next_result();
		return $mediaList; */
	}
	
	
	function mediaGetType($typeID){
		$rs = $this->query( "CALL media_get_type(".$typeID.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		return $row;
	}
	
	function mediaGetParentID($mediaID){
		if($mediaID > 0){
			$rs = $this->query( "CALL media_get_parent_id(".$mediaID.")" );
			$row = $rs->fetch_object();
			$this->next_result();
			return $row->parent_id;
		}
		return 0;
	}
	
	function mediaGetAlbumPath($mediaID){
		$path = "";
		if($mediaID > 0){
			$path = $mediaID;
		}
		$currentMediaID = $mediaID;
		while($currentMediaID != 0){
			$rs = $this->query( "CALL media_get_parent_id(".$currentMediaID.")" );
			$row = $rs->fetch_object();
			$this->next_result();
			$currentMediaID = $row->parent_id;
			if ($currentMediaID != 0){
				$path = $currentMediaID. DIRECTORY_SEPARATOR .$path;
			}
		}
		return $path;
	}
	
	
	function mediaDeleteByMediaID($PageID,  $iUserID){
		$rs = $this->query( "CALL media_delete_by_media_id(".$PageID.", ".$iUserID.")" );
//		echo "CALL media_delete_by_media_id(".$PageID.", ".$iUserID.")";
		$pageChildrenIDs = array();
		while($row = $rs->fetch_assoc()){
			array_push($pageChildrenIDs, $row['id']);
		}
		$this->next_result();
		$childPages = count($pageChildrenIDs);
		foreach($pageChildrenIDs as $pageChildID){
			$childPages += $this->mediaDeleteByMediaID($pageChildID, $iUserID);
		}
		return $childPages;
	}
	
	
	/**** Users  ****/
	function getUserByID($user_id){
		$rs = $this->query( "CALL get_user_by_id(".$user_id.")" );
		$row = $rs->fetch_object();
		$this->next_result();
		$user = new User($row);
		return $user;
	}
	
	function userGetCurrent(){
		if(!isset($_SESSION)){
			session_start();
		}
		$rs = $this->query( "CALL user_get_current('".$_SESSION['session']."')" );
		$row = $rs->fetch_object();
		$this->next_result();
		$user = new User($row);
		return $user;
	}
	
	
	/**** Library  ****/
	function libraryGetBookList($PageID, $Rows, $Sort, $Order){
		$rs = $this->query( "CALL library_getOrderedBookList(".$PageID.", ".$Rows.", ".$Sort.", ".$Order.")" );
		$bookList = array();
		while($row = $rs->fetch_assoc()){
			array_push($bookList, $row);
		}
		$this->next_result();
		/*$rs = $this->query( "SELECT COUNT (Id) Books" );
		$count = $rs->fetch_field();
		$this->next_result();*/
		return array('rows' => $bookList, 'total' => 3);
	}


}


?>