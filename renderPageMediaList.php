<?php 
echo $currentPage->Content;
$mediaIDList = $db->pageGetMediaIDList($currentPage->PageID, 1);
?><ul class="mediaList"> <?php		
foreach($mediaIDList as $mediaID ){
	
	$mediaItem = $db->mediaGetByID($mediaID); 
	$author = $db->mediaGetInfoDocsByID($mediaID)->Author;
	?>
    <li id="sideMedia_<?php echo $mediaID; ?>"><a href="cms/media/original/<?php echo $mediaItem->FullPath; ?>"><?php  echo $mediaItem->Title; if (strlen($author) > 0){?><span class="mediaAuthor"> (<?php  echo $author; ?>)</span><?php } ?></a></li>
<?php }

?>
</ul>