<?php
$pagelist = $db->pageGetListAtLevel($currentPage->PageID);

function stripArgumentFromTags( $htmlString ) {
    $regEx = '/([^<]*<\s*[a-z](?:[0-9]|[a-z]{0,9}))(?:(?:\s*[a-z\-]{2,14}\s*=\s*(?:"[^"]*"|\'[^\']*\'))*)(\s*\/?>[^<]*)/i'; // match any start tag

    $chunks = preg_split($regEx, $htmlString, -1,  PREG_SPLIT_DELIM_CAPTURE);
    $chunkCount = count($chunks);

    $strippedString = '';
    for ($n = 1; $n < $chunkCount; $n++) {
        $strippedString .= $chunks[$n];
    }

    return $strippedString;
}


//echo $currentPage->Content;
foreach($pagelist as $tPage){  
	$bPage = $db->pageGetByPageIDandLanguageCode($tPage->PageID, 'en');
	//$images = glob("res/images/page_images/p".$currentPage->PageID . "/*.jpg");
	//print_r($tPage);
				$images = $db->pageGetMediaIDList($tPage->PageID);
				 
				//print each file name
				if(count($images) > 0){
				$image = $images[0];
				
					$mediaItem = $db->mediaGetByID($image); 
				}
					echo '<a href="'.$bPage->PermaLink.'" class="newsPost">';
					if(count($images) > 0){ 
						echo '<div class="newsThumb"><img alt="" src="'.$mediaItem->getCustomURL("tribunalNews", 264, 150, "cms/").'"/></div>';
					}
	
		?>
    <div class="newsTitle"><?php echo $bPage->Title; ?></div>
    <div class="newsDate"><?php echo date ("j F Y", $bPage->TimeCreated); ?></div>
    <div><?php $stripedContent = stripArgumentFromTags($bPage->FullPage->content); echo substr($stripedContent, 0, 350); if(strlen($stripedContent) > 350) { echo " ..."; } ?></div></a>
<?php }	?>
