<?PHP
	ob_start("ob_gzhandler");
	$toParse = $_GET['i'];
	if (strlen($toParse) <= 0) {
		/*
		$type = $_GET['t'];
		$size = $_GET["s"];
		$thumbId = $_GET["id"];
		$folder = $_GET["f"];
		*/
	} else {
		$type = substr($toParse, -1, 1);
		$size = substr($toParse, -2, 1);
		$thumbId = substr($toParse, 0, 36);
		$folder = substr($toParse, 37, strlen($toParse)-40);
		$folder = str_replace("-", "/", $folder);
	}
	switch ($type) {
		case 'u':
			$type = "Users";
			break;
		case 'c':
			$type = "Consultants";
			$folder = "";
			break;
		default:
			$type = "Properties";
			break;
	}
	switch ($size) {
		case 'c':
			$size = "CrossSlide";
			break;
		case 'l':
			$size = "Lightbox";
			break;
		case 'm':
			$size = "Medium";
			break;
		default:
			$size = "Thumb";
			break;
	}
	$file = $type."/".$folder.$size.'/'.$thumbId.'.jpg';
	if (file_exists($file)) {
		$filename = $file;
	} else {
		$filename = $type."/".$size.'/default.jpg';
	}
	header('Content-type: image/jpeg');
	header('Expires: '.date("r", mktime(0, 0, 0, 7, 1, date("Y")+1)));
	header('Cache-Control: public');
	header('Content-Version: 1');
	//header('Content-MD5: '.md5_file($filename));
	header('If-Modified-Since: '.date('r', filemtime($filename)));
	header('Last-Modified: '.date('r', filemtime($filename)));
	readfile($filename);
?>