<?php

if(isset($_GET['es'])){
	session_start();
	session_destroy();
}

include_once('includes/cms.php');
//include_once('includes/SQL.class.php');
//include_once('includes/Pages.class.php');
//$Security = new Security();

$requestURI = explode('/', $_SERVER['REQUEST_URI']);
$permaLink = $requestURI[1];

if(strlen($permaLink) == 0){
	$permaLink = 'home';
}



$db = new SQL();
$tmpPage = $db->pageGet($permaLink,'en');
//$currentPage = object();
if($tmpPage != false){
	$currentPage = new PageStatic($tmpPage->FullPage);
}else{
	header('Location: 404.php?from='.$_SERVER['REQUEST_URI']);
}

function writeHref($link){
	global $currentPage, $db;
	if($currentPage->RootID > 0){
		if ($db->pageGetByPageIDandLanguageCode($currentPage->RootID, 'en')->PermaLink == $link){
			echo 'class="active" ';
		}
	}else{
		if($currentPage->PermaLink == $link){
			echo 'class="active" ';
		}
	}
	
	
	
	
	echo 'href="'.$link.'"';
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo  strip_tags($currentPage->Title); ?></title>
<link href="css/main.css" rel="stylesheet" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Oswald:300' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js" type="text/javascript"></script>
<script>
	$(function(){
		$('.bannerImages img:gt(0)').hide();
		setInterval(function(){
		  $('.bannerImages img:first').fadeOut(2000, "linear", function(){})
			 .next('img').fadeIn(2000, "linear", function(){})
			 .end('img').appendTo('.bannerImages');}, 
		  10000);
	});

</script>
</head>

<body>
<div class="headerLine"></div>
<div id="pageContainer">
	<div id="header">
    	<div class="mainMenu">
        	<img alt="Logo" src="res/images/logo.jpg" />
            <ul >
                <li><a <?php writeHref('home'); ?>>Home</a></li>
                <li><a <?php writeHref('1st-instance'); ?>>1<sup>st</sup> Instance</a></li>
                <li><a <?php writeHref('2nd-instance'); ?>>2<sup>nd</sup> Instance</a></li>
                <li><a <?php writeHref('church-laws'); ?>>Church Laws</a></li>
                <li><a <?php writeHref('faqs'); ?>>FAQs</a></li>
                <li><a <?php writeHref('contact'); ?>>Contact Us</a></li>
            </ul>
        </div>
        <div class="banner">
        	<div class="bannerImages">
            	<?php 
					$bannerImages = glob("res/images/banner/*.jpg");
					foreach($bannerImages as $image)
					{
						echo '<img alt="" src="'.$image.'" />';
					}
				?>
            </div>
            <div class="bannerBG" ></div>
        </div>
        <div class="crumbar"><div class="crums"><?php 
			if($currentPage->RootID > 0){
				$parentPage = $db->pageGetByPageIDandLanguageCode($currentPage->RootID, 'en');
				echo '<a href="'.$parentPage->PermaLink.'" >'.strtoupper($parentPage->Title)."</a><span></span>";
			}
			echo strtoupper(strip_tags($currentPage->Title)); 
			
		?></div><img  alt="" src="res/images/crum_title.jpg" /></div>
    </div>
    <div id="pageContent">
    	<div id="pageLeft">
        	<img id="leftImageTopPadding" src="res/images/page_image_BG_top.jpg" />
            <?php 
				//$images = glob("res/images/page_images/p".$currentPage->PageID . "/*.jpg");
				$images = $db->pageGetMediaIDList($currentPage->PageID);
				 
				//print each file name
				foreach($images as $image)
				{
					$mediaItem = $db->mediaGetByID($image); 
					echo '<div><img alt="" src="'.$mediaItem->getCustomURL("tribunal", 264, 0, "cms/").'" /></div>';
				}
				if(count($images) == 0){
					echo '<img alt="" src="res/images/page_image_BG_noImage.jpg" />';
				}
			?>
            
        </div>
        <div id="pageMiddle">
        	<div id="pageTitle"><?php echo $currentPage->Title; ?></div>
            <div id="pageText">
            	<?php echo $currentPage->Content; ?>
            </div>
        </div>
        <div id="pageRight">
        	<ul>
                <?php 
					if($currentPage->ParentID == 0){
						$submenuList = $db->pageGetSubmenuLinks($currentPage->PageID, 1);
					}else{
						$submenuList = $db->pageGetSubmenuLinks($currentPage->ParentID, 1);
					}
					foreach($submenuList as $subEntry){
						$link = '';
						echo '<li><a href="';
						if ($subEntry->type == 0){
							if($subEntry->external == 0){
								$link = $subEntry->link;
							}else{
								$link = $subEntry->link;
							}
						}else{
							$link = $subEntry->permalink;
						}
						echo $link.'"';
						if($link == $currentPage->PermaLink){
							echo ' class="active"';
						}
						echo '>'.$subEntry->title.'</a></li>';
					}
				?>
            </ul>
        </div>
    </div>
</div>

<div id="footer">
    <div class="headerLine"></div>
    <div id="footerLinks">
    	<a href="http://www.vatican.va/" target="_new">VATICAN</a>::
        <a href="http://www.maltachurch.org.mt/">ARCHIDIOCESE OF MALTA</a>::
        <a href="#">DIOCESE OF GOZO</a>::
        <a href="#">BIBLE</a>
    </div>
    <div id="footerTerms">
    	<div class="left">
        	<a href="copyright">Copyright &copy; <?php date_default_timezone_set('Europe/Paris'); echo date('Y') ?></a>&bull;
            <a href="privacy-policy">Privacy Policy</a>&bull;
            <a href="legal-disclaimer">Legal Disclaimer</a>
        </div>
        <div class="right">
        	<a href="#" >developed and hosted by Nyx Solutions</a>
        </div>
    </div>
</div>

</body>
</html>