<?PHP

	ob_start("ob_gzhandler");
	$requestURI = explode('/', $_SERVER['REQUEST_URI']);
	//print_r($requestURI);
	//$toParse = $_GET['i'];
	$toParse = $requestURI[2];
	if (strlen($toParse) <= 0) {
		/*
		$type = $_GET['t'];
		$size = $_GET["s"];
		$thumbId = $_GET["id"];
		$folder = $_GET["f"];
		*/
	} else {
		$type = substr($toParse, -1, 1);
		$size = substr($toParse, -2, 1);
		$thumbId = substr($toParse, 0, 36);
		$folder = substr($toParse, 37, strlen($toParse)-40);
		$folder = str_replace("-", "/", $folder);
	}
	switch ($type) {
		case 'u':
			$type = "users";
			break;
		case 'c':
			$type = "consultants";
			$folder = "";
			break;
		default:
			$type = "properties";
			break;
	}
	switch ($size) {
		case 'c':
			$size = "crossslide";
			break;
		case 'l':
			$size = "lightbox";
			break;
		case 'm':
			$size = "medium";
			break;
		default:
			$size = "thumb";
			break;
	}
	$file = $type."/".$folder.$size.'/'.$thumbId.'.jpg';
	if (file_exists($file)) {
		$filename = $file;
		header('Expires: '.date("r", mktime(0, 0, 0, 7, 1, date("Y")+1)));	
		header('Content-Version: 2');
	} else {
		$filename = $type."/".$size.'/default.jpg';
		header('Expires: '.date("r", mktime(21, 0, 0, date("m"), date("d"), date("Y"))));	
		header('Content-Version: 1');
	}

	$modDate = date('r', filemtime($filename));
	header('Content-type: image/jpeg');
	
	header('Cache-Control: public');
	//header('Content-MD5: '.md5_file($filename));
	header('If-Modified-Since: '.$modDate);
	header('Last-Modified: '.$modDate);
	readfile($filename);
?>