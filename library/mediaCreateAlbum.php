<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete
/*
	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');*/
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();

if(isset($_POST['title'])){
	$newAlbumID = $db->mediaCreate( $_POST['title'], $_POST['parentID'], $currentUser->ID, 0, $_POST['state']);
	header('Location: media.php?pid='.$newAlbumID);
}


$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->mediaGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->mediaGetParentID($page_parent_id);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
function checkPermalink(){
	$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-").toLowerCase());
		showActivityMsg("Checking permalink validity");
		$.ajax({
		  type: "POST",
		  url: "ajax/pageGetNextPermalink.php",
		  data: "permalink="+$("#permaLink").val()
		}).done(function( msg ) {
			$("#permaLink").val(msg);
		  //alert( "Data Saved: " + msg );
		});
}

$(function(){
	$("#title").keyup(function(){
		$("#permaLink").val($("#title").val().replace(/ /gi, "-").toLowerCase());
	});
	
	$("#title").change(function(){
		checkPermalink();
	});
	
	
	$("#permaLink").keyup(function(){
		$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-").toLowerCase());
	});
	
	$("#permaLink").change(function(){
		checkPermalink();
	});
});
</script>
<title>Create New Page</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title">Create New Album<span class="btnHolder">
        	<a href="media.php?pid=<?php echo $page_parent_id; ?>" ><img src="res/images/folder_up.png" /></a>
        </span></div>
        <form id="pageCreate" method="post" action="mediaCreateAlbum.php">
            <label>Title:
	            <input type="text" id="title" name="title" />
            </label>
           
            <label>Status:
                <select name="state" >
                    <option value="0">Draft</option>
                    <option value="1" selected="selected">Published</option>
                </select>
            </label>
          	
            <input type="hidden" name="parentID" value="<?php echo $page_parent_id; ?>" />
            <input type="submit"  class="action_btn" value="Create Album" />
        </form>
      
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>