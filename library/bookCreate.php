<?php

$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();


//to delete

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
/*
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);
*/

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>
<?PHP 
$id = 0;
$title = "";
$year = "";
$volume = "";
$shelfNo = "";
$language = "";
$status = "";

$action = "Add New";


if (isset($_GET["id"])) {
	$action = "Edit";
	$row = $db->libraryGetBook($_GET["id"]);
	$id = $_GET["id"];
	$title = $row->title;
	$year = $row->year;
	$volume = $row->volume;
	$shelfNo = $row->shelf_id;
	$language = $row->language_id;
	$status = $row->status_id;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<title><?php echo $action ?> Book</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>

<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title"><?php echo $action ?> Book<span class="btnHolder">
        	<a href="index.php" ><img src="res/images/document_shred.png" /></a>
        </span></div>
        <form id="pageCreate" method="post" action="bookUpdate.php" onsubmit="return validation_test(this)">
			<input type="hidden" id="id" name="id" value="<?php echo $id ?>" />
            <label>Title:
	            <input class="validate" _validatefnc="required" _validateProperty="Title" type="text" id="title" name="title" value="<?php echo $title ?>" />
            </label>
            <label>Year:
	            <input type="text" id="year" name="year" value="<?php echo $year ?>" />
            </label>
            <label>Volume:
	            <input type="text" id="volume" name="volume" value="<?php echo $volume ?>" />
            </label>
            <label>Shelf No:
				<select name="shelfID">
                	<?php 
						$allBookStatuses = $db->bookGetAllShelves();
						foreach ($allBookStatuses as $bStatus){ 
							if ($bStatus->id > 0){
							echo '<option '.($shelfNo == $bStatus->id ? 'selected="selected" ' : '').' value="'.$bStatus->id.'">'.$bStatus->name.'</option>';
					 } }?>
                </select>
            </label>
            <label>Language:
                <select name="languageID">
                    <option value="1">English</option>
                </select>
            </label>
            <label>Status:
                <select name="statusID">
                	<?php 
						$allBookStatuses = $db->bookGetAllStatuses();
						foreach ($allBookStatuses as $bStatus){ 
							if ($bStatus->id > 0){
							echo '<option '.($status == $bStatus->id ? 'selected="selected" ' : '').' value="'.$bStatus->id.'">'.$bStatus->name.'</option>';
					 } }?>
                </select>
            </label>
            <input type="submit"  class="action_btn" value="<?php echo $action ?> Book" />
        </form>
      
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>