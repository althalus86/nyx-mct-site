<?php

$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();


//to delete

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
/*
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);
*/

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>
<?PHP 
$id = 0;
$book = $_GET["book"];
$clientId = "";
$clientName = "";
$dateTaken = "";
$dateReturned = "";
$action = "Add New";


if (isset($_GET["id"])) {
	$action = "Edit";
	$row = $db->libraryGetBookLending($_GET["id"]);
	$id = $_GET["id"];
	$book = $row->bookId;
	$clientId = $row->client_id;
	$clientName = $row->client_name.' '.$row->client_surname;
	$dateTaken = $row->dateTaken;
	$dateReturned = $row->dateReturned;

	if ($dateTaken > 0) {
		$dateTaken = date("Y-m-d", $dateTaken);
	} else {
		$dateTaken = "";
	}
	
	if ($dateReturned > 0) {
		$dateReturned = date("Y-m-d", $dateReturned);
	} else {
		$dateReturned = "";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<title><?php echo $action ?> Book Lending</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>

<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title"><?php echo $action ?> Book Lending<span class="btnHolder">
        	<a href="bookDetails.php?id=<?php echo $book; ?>" ><img src="res/images/document_shred.png" /></a>
        </span></div>
        <form id="pageCreate" method="post" action="bookLendingUpdate.php" onsubmit="return validation_test(this)">
			<input type="hidden" id="id" name="id" value="<?php echo $id ?>" />
            <input type="hidden" id="book" name="book" value="<?php echo $book ?>" />
            <label>Client:
	            <input class="validate" _validatefnc="required" _validateProperty="Client" type="hidden" id="clientId" name="clientId" value="<?php echo $clientId ?>" />
				<input type="text" id="clientName" value="<?php echo $clientName ?>" />
            </label>
            <label>Date Taken:
	            <input type="text" class="datepicker validate" id="dateTaken" name="dateTaken" value="<?php echo $dateTaken ?>" />
            </label>
            <label>Date Returned:
				<input type="text" class="datepicker" id="dateReturned" name="dateReturned" value="<?php echo $dateReturned ?>" />
            </label>
            <input type="submit"  class="action_btn" value="<?php echo $action ?> Book Lending" />
        </form>
      
    </div>
</div>
<script>
	$(function() {
		$( "#dateTaken" ).datepicker({ dateFormat: "yy-mm-dd", onSelect: function() { 
				$( "#dateReturned" ).datepicker("option", "minDate", $( "#dateTaken" ).datepicker( "getDate" ));
			}
		});
		$( "#dateReturned" ).datepicker({ dateFormat: "yy-mm-dd"});
		$( "#dateReturned" ).datepicker("option", "minDate", $( "#dateTaken" ).datepicker( "getDate" ));
		$( "#clientName" ).autocomplete({
		  source: "data/clients_search.php",
		  minLength: 2,
		  select: function( event, ui ) {
			$( "#clientId" ).val(ui.item.id);
		  }
		});
	});
</script>
<div id="activityBar">loading...</div>
</body>
</html>