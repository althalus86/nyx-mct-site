<?php
//ini_set('display_errors', '1');
//error_reporting(E_ALL);
$toRoot = "../../";
include_once($toRoot.'includes/cms.php');

$db = new SQL();

$filters = (json_decode($_POST["filterRules"]));
$search = "";
if ($filters != null) {
    for ($i = 0; $i < sizeof($filters); $i++) {
        $search .= " AND " . $filters[$i]->field . ' LIKE "%' . $filters[$i]->value . '%" ';
    }
}
$result = $db->libraryGetBookFullList($_POST["page"], $_POST["rows"], $_POST["sort"], $_POST["order"], $search);
echo json_encode($result);
?>