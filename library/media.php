<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete
/*
	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');*/
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$languageCode = "en";

$db = new SQL();
$media_parent_id = 0;
if (isset($_GET['pid'])){
	$media_parent_id = $_GET['pid'];
}
$medialist = $db->mediaGetListAtLevel($media_parent_id);

$media_parent_parent_id = $db->mediaGetParentID($media_parent_id);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
	$(function() {
		$( "#selectable" ).selectable({ 
			//filter: "li.media",  
			stop: function(event, ui) { 
				if($('#selectable li.ui-selected').length < 2){
					if($('#selectable li.ui-selected.album').length == 1){
						window.location = $('#selectable li.ui-selected.album a').attr('href');
					}
				}
			
				checkDeleteBtnOpactiy(); 
				
			}
		});
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( "#dialog-confirm" ).dialog({
			autoOpen: false,
			resizable: false,
			width:400,
			height:220,
			modal: true,
			buttons: {
				"Delete all items": function() {
					var pagesToDelete = "";
					$('#selectable li.ui-selected').each(function (index){
						pagesToDelete += $(this).attr("id").substr(6)+",";
					});
					pagesToDelete = pagesToDelete.substr(0, pagesToDelete.length -1);
					$.ajax({
					  type: "POST",
					  url: "ajax/mediaDeleteListByMediaID.php",
					  data: "pageIdList="+pagesToDelete+"&mediaID=<?php if(isset($_GET['pid'])){ echo $_GET['pid']; }else{ echo 0;} ?>"
					}).done(function( msg ) {
						showActivityMsg(msg);
						$('#selectable li.ui-selected').each(function (index){
							$(this).remove();
						});
						checkDeleteBtnOpactiy();
					});
					//alert(pagesToDelete);
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					
					$( this ).dialog( "close" );
				}
			}
		});
		
		checkDeleteBtnOpactiy();
		
		$( "#sortable" ).sortable({ axis: 'y', opacity: 0.6,  handle: 'span'  });
		$( "#sortable" ).disableSelection();
		$( "#sortable" ).bind( "sortupdate", function(event, ui) {
				var ids = $("#sortable").sortable('serialize');
			  	$.ajax({
				  type: "POST",
				  url: "ajax/saveOrderPageList.php",
				  data: ids
				}).done(function( msg ) {
					showActivityMsg("Sorting has been saved");
				  //alert( "Data Saved: " + msg );
				});
		});
	});
	
	function deleteSelected(){
		if($('#selectable li.ui-selected').length > 0){
			$( '#dialog-confirm' ).dialog( 'open' );
		}
	}
	
	function checkDeleteBtnOpactiy(){
		if($('#selectable .ui-selected').length > 0){
			$('#pagesDeleteBtn').css('opacity','1');
		}else{
			$('#pagesDeleteBtn').css('opacity','0.2');
		}
	}
	
	function mediaShowInfo(mediaID){
		$('.ui-selected').removeClass('ui-selected ui-selecting');
		
		$( "#dialog-media-info" ).dialog({
			height: ($(document).height() * 0.8),
			width: "80%",
			modal: true
		});
		$( "#dialog-media-info iframe" ).attr("src", ($( "#dialog-media-info iframe" ).attr("_src")+""+mediaID));
		$('.ui-selecting').removeClass('ui-selected ui-selecting');
		$('.ui-selected').removeClass('ui-selected ui-selecting');
	}
	
	
</script>
<title>Media</title>

</head>

<body>
<div id="dialog-confirm" title="Delete Selected Media?">
	<p><span style="float:left; margin:50px 20px 50px 0px;"><img src="res/images/warning.png" /></span>These items will be permanently deleted and cannot be recovered.<br /><br /> All nested children will be deleted as well.<p style="text-align:center; font-style:italic; font-weight:bold;">Are you sure?</p></p>
</div>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"><?php $sideBarItem = 1; include("res/php/sidebar.php"); ?></div>
    <div class="page">
        <div class="title">Media<span class="btnHolder">
			<?php if($media_parent_id > 0){ echo '<a href="media.php?pid='.$media_parent_parent_id.'" ><img src="res/images/folder_up.png" /></a>'; } ?>
            
            <a href="mediaCreateAlbum.php?pid=<?php echo $media_parent_id; ?>" ><img src="res/images/folder_add.png" /></a>
        	<a href="mediaCreate.php?pid=<?php echo $media_parent_id; ?>" ><img src="res/images/media_add.png" /></a>
            <a href="#" onclick="deleteSelected();" ><img id="pagesDeleteBtn" src="res/images/media_delete.png" /></a>
        </span></div>
        <?php if(count($medialist) > 0){ ?>
        <ol id="selectable">
            <?php foreach ($medialist as $bMedia){ ?>
                    <li class="ui-state-default <?php if ($bMedia->getmediaType()->ID <> 0) { echo "media"; } else { echo "album"; } ?>" id="media_<?php echo $bMedia->ID;  ?>" >
						<?php if ($bMedia->getmediaType()->ID == 0) { ?>
                        			<a  href="media.php?pid=<?php echo $bMedia->ID;  ?>" ><img src="res/images/album.png" /><span class="media_title"><?php echo $bMedia->Title; ?></span></a>
							  <?php } elseif($bMedia->getmediaType()->ID == 1) { ?>
						         	<a  href="media.php?pid=<?php echo $bMedia->ID;  ?>" ><img style="margin-top:10px;" src="<?php  echo $bMedia->getThumbURL(); ?>" /><span class="media_title"><?php echo $bMedia->Title; ?></span><div class="media_infoBtn" onmousedown="mediaShowInfo(<?php echo $bMedia->ID;  ?>);" ></div></a> 
							<?php  }else{ ?>
                            		<a  href="media.php?pid=<?php echo $bMedia->ID;  ?>" ><img style="margin-top:10px;" width="140" src="<?php   echo $bMedia->getFileTypeIconURL(); ?>" /><span class="media_title"><?php echo $bMedia->Title; ?></span><div class="media_infoBtn"  onmousedown="mediaShowInfo(<?php echo $bMedia->ID;  ?>);"></div></a> 
                            <?php  } ?>
                    </li>
            <?php }	?>
        </ol>
        <?php }else{ echo '<div class="pageMsg">This folder has no media yet.</div>';} ?>
    </div>
</div>

<div id="dialog-media-info" title="Media Information" style="display:none">
	<iframe width="100%" frameborder="0" _src="mediaInfoDocs.php?mediaID="> </iframe>
</div>

<div id="activityBar">loading...</div>
</body>
</html>