<?php

$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();


//to delete

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
/*
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);
*/

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>
<?PHP 
$id = 0;
$name = "";
$surname = "";
$idCard = "";
$tel = "";
$mob = "";
$email = "";

$action = "Add New";


if (isset($_GET["id"])) {
	$action = "Edit";
	$row = $db->libraryGetClient($_GET["id"]);
	$id = $_GET["id"];
	$name = $row->name;
	$surname = $row->surname;
	$idCard = $row->idcard;
	$mob = $row->mobile;
	$tel = $row->telephone;
	$email = $row->email;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<title><?php echo $action ?> Client</title>
<script>
$(function() {
		//easyloader.load("datagrid", function(){
			$('#tt').datagrid({  
				url:'data/clients_load.php', 
				onDblClickRow: function(rowIndex, rowData) { 
					window.location = "clientCreate.php?id="+rowData.id;
				}
			});
		//});
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( "#dialog-confirm" ).dialog({
			autoOpen: false,
			resizable: false,
			width:400,
			height:220,
			modal: true,
			buttons: {
				"Delete": function() {
					$.ajax({
					  type: "POST",
					  url: "ajax/clientDeleteById.php",
					  data: "id=<?php echo $_GET["id"] ?>"
					}).done(function( msg ) {
						showActivityMsg(msg);
						window.location = "clients.php";
					});
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					
					$( this ).dialog( "close" );
				}
			}
		});
		
		
		$( "#sortable" ).sortable({ axis: 'y', opacity: 0.6,  handle: 'span'  });
		$( "#sortable" ).disableSelection();
		$( "#sortable" ).bind( "sortupdate", function(event, ui) {
				var ids = $("#sortable").sortable('serialize');
			  	$.ajax({
				  type: "POST",
				  url: "ajax/saveOrderPageList.php",
				  data: ids
				}).done(function( msg ) {
					showActivityMsg("Sorting has been saved");
				  //alert( "Data Saved: " + msg );
				});
		});
	});
	
	function deleteClient(){
		$( '#dialog-confirm' ).dialog( 'open' );
	}
</script>
</head>

<body>
<div id="dialog-confirm" title="Delete Client?">
	<p><span style="float:left; margin:50px 20px 50px 0px;"><img src="res/images/warning.png" /></span>This item will be permanently deleted and cannot be recovered.<p style="text-align:center; font-style:italic; font-weight:bold;">Are you sure?</p></p>
</div>

<?php $sBarUI = new StatusBar();  ?>

<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title"><?php echo $action ?> Client<span class="btnHolder">
        	<a href="clients.php" ><img src="res/images/document_shred.png" /></a>
			<a href="#" onclick="deleteClient();" ><img id="pagesDeleteBtn" src="res/images/page_delete.png" /></a>
			</span></div>
        <form id="pageCreate" method="post" action="clientUpdate.php" onsubmit="return validation_test(this)">
			<input type="hidden" id="id" name="id" value="<?php echo $id ?>" />
            <label>Name:
	            <input class="validate" _validatefnc="required" _validateProperty="Name" type="text" id="name" name="name" value="<?php echo $name ?>" />
            </label>
            <label>Surname:
	            <input class="validate" _validatefnc="required" _validateProperty="Surname" type="text" id="surname" name="surname" value="<?php echo $surname ?>" />
            </label>
            <label>ID Card:
	            <input type="text" id="idcard" name="idcard" value="<?php echo $idCard ?>" />
            </label>
            <label>Telephone:
				<input type="text" id="tel" name="tel" value="<?php echo $tel ?>" />
            </label>
            <label>Mobile:
                <input class="validate" _validatefnc="required" _validateProperty="Mobile" type="text" id="mob" name="mob" value="<?php echo $mob ?>" />
            </label>
            <label>Email:
                <input class="validate" _validatefnc="required" _validateProperty="Email" type="text" id="email" name="email" value="<?php echo $email ?>" />
            </label>
            <input type="submit"  class="action_btn" value="<?php echo $action ?> Client" />
        </form>
      
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>