<img src="res/images/logo.png" />
<script>
	function showDiag(type){
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		
		if(type == 'doc'){
			$( "#dialog-doc-browser" ).dialog({
				height: ($(document).height() * 0.8),
				width: "80%",
				modal: true,
				
			});
		}else{
			$( "#dialog-media-browser" ).dialog({
				height: ($(document).height() * 0.8),
				width: "80%",
				modal: true,
				
			});
		}
		
	}
	
	function removePageMedia(pageID, mediaID){
		
		$.ajax({
		  type: "POST",
		  url: "ajax/pageDeleteMedia.php",
		  data: "mediaID="+mediaID+"&pageID="+pageID
		}).done(function( msg ) {
			if(msg.length > 0){
				showActivityMsg(msg);
				$('#sideMedia_'+mediaID).remove();
			}
		});
	}
	
	function addPageMedia(mediaID, mediaURL, mediaTitle, group ){
		 group = typeof group !== 'undefined' ? group : 0;
		switch (group){
			case 0:
				$("#sideImages").append('<li id="sideMedia_'+mediaID+'"><img src="'+mediaURL+'" /><div><span>'+mediaTitle+'</span><span class="sideMedia_remove" onClick="removePageMedia(<?php echo $currentPage->PageID.", "; ?>'+mediaID+')">remove</span></div></li>');
				break;
			case 1:
				$("#sideDocs").append('<li id="sideMedia_'+mediaID+'"><img style="border:0;" width="45" src="'+mediaURL+'" /><div><span class="sideMediaTitle">'+mediaTitle+'</span><span class="sideMedia_remove" onClick="removePageMedia(<?php echo $currentPage->PageID.", "; ?>'+mediaID+')">remove</span></div></li>');
				break;
		}
	}
</script>
<div>
	<div id="sideImage_title">Side Images</div>
    <ul id="sideImages" class="side_images">
    	<?php 
		$mediaIDList = $db->pageGetMediaIDList($currentPage->PageID);
		
		foreach($mediaIDList as $mediaID ){
			
			$mediaItem = $db->mediaGetByID($mediaID); 
			//echo $mediaItem->getCustomURL("sidethumbs", 45, 0);
			?>
            
            <li id="sideMedia_<?php echo $mediaID; ?>"><img src="<?php  echo $mediaItem->getCustomURL("sidethumbs", 45, 35, "" , "fitin"); ?>" /><div><span class="sideMediaTitle"><?php  echo $mediaItem->Title; ?></span><span class="sideMedia_remove" onClick="removePageMedia(<?php echo $currentPage->PageID.", ".$mediaID; ?>)">remove</span></div></li>
		<?php }	?>
    	
    </ul>
</div>
<input type="button" style="margin:10px;" onClick="showDiag('img')" value="Add Image" />
<div>
	<div id="sideImage_title">Attach Documents</div>
    <ul id="sideDocs" class="side_images">
    	<?php 

		$mediaIDList = $db->pageGetMediaIDList($currentPage->PageID, 1);
		
		foreach($mediaIDList as $mediaID ){
			
			$mediaItem = $db->mediaGetByID($mediaID); 
			//echo $mediaItem->getCustomURL("sidethumbs", 45, 0);
			?>
            
            <li id="sideMedia_<?php echo $mediaID; ?>"><img style="border:0;" width="45" src="<?php  echo $mediaItem->getFileTypeIconURL(); ?>" /><div><span class="sideMediaTitle"><?php  echo $mediaItem->Title; ?></span><span class="sideMedia_remove" onClick="removePageMedia(<?php echo $currentPage->PageID.", ".$mediaID; ?>)">remove</span></div></li>
		<?php }	?>
    	
    </ul>
</div>
<input type="button" style="margin:10px;" onClick="showDiag('doc')" value="Add Document" />
<div>
    <div id="sideImage_title">Order By</div>
    <select id="orderBy" name="orderBy">
        <option <?php if($currentPage->OrderBy == 0){ echo "selected";} ?> value="0">Unordered</option>
        <option <?php if($currentPage->OrderBy == 1){ echo "selected";} ?> value="1">Order By Date Desc</option>
        <option <?php if($currentPage->OrderBy == 2){ echo "selected";} ?> value="2">Order Alphabetically</option>
    </select>
</div>
<div id="dialog-media-browser" class="dialog-media-browser" title="Media Browser" style="display:none">
	<iframe width="100%" frameborder="0" src="media_browser.php?only_exts=jpeg,jpg&only_types=1&pageID=<?php if (isset($_GET['pg'])){echo $_GET['pg']; }else{echo 0;}?>"> </iframe>
</div>

<div id="dialog-doc-browser" class="dialog-media-browser" title="Media Browser" style="display:none">
	<iframe width="100%" frameborder="0" src="media_browser.php?only_exts=pdf&only_types=2&group=1&pageID=<?php if (isset($_GET['pg'])){echo $_GET['pg']; }else{echo 0;}?>"> </iframe>
</div>

