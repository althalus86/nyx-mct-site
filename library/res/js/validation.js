var validation_errorList = "";

function validation_required(elem, kname) {
	if (elem.value.length > 0) {
		return true;
	}
	validation_errorList += kname+" is required<br />";
	return false;
}

function validation_test(elem) {
	validation_errorList = "";
	var valid = true;
	$(elem).find(".validate").each(function() {
		var v = eval("validation_"+$(this).attr("_validatefnc"))(this, $(this).attr("_validateProperty"));
		if (!v) { valid = false; }
	});
	if (!valid) {
		showActivityMsg(validation_errorList);
	}
	return valid;
}