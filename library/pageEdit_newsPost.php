<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete
/*
	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');*/
//to delete

$db = new SQL();
$currentUser = $db->userGetCurrent();

if(isset($_POST['permaLink'])){
	$languagePageID = $db->pageCreate($_POST['permaLink'], $_POST['languageID'], $_POST['title'], $_POST['parentID'], $currentUser->ID, $_POST['typeID'], $_POST['state']);
	//echo $languagePageID;
	$staticPageID = $db->pageCreateNewsPost($languagePageID, "");
	$currentPage = new PageStatic($db->pageGetByLanguageID($languagePageID)->FullPage);
}else if (isset($_GET['lc'])){
	$currentPage = new PageStatic($db->pageGetByPageIDandLanguageCode($_GET['pg'], $_GET['lc'])->FullPage);
}


$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script src="res/plugins/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script src="res/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script> 
<script>
$(function(){
	$(window).resize(function(){
		CKEDITOR.instances.contentEditor.resize("100%",$('body').innerHeight()-120);
		//CKEDITOR.config.height = $('body').innerHeight()-260;
	});
	
	$('.title input').attr('size',$('.title input').val().length);
	CKEDITOR.config.height = $('body').innerHeight()-255;
	
	//$('.title input').css('width',$('.title input').val().length*15+'px'); 
//	CKEDITOR.instances.contentEditor.resize("100%",$('body').innerHeight()-110);
	
	$("#title").keyup(function(){
		$("#permaLink").val($("#title").val().replace(/ /gi, "-"));
	});
	
	
	$("#permaLink").keyup(function(){
		$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-"));
	});
	
	$("#permaLink").change(function(){
		$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-"));
		$.ajax({
		  type: "POST",
		  url: "ajax/pageGetNextPermalink.php",
		  data: "permalink="+$("#permaLink").val()
		}).done(function( msg ) {
			$("#permaLink").val(msg);
		  //alert( "Data Saved: " + msg );
		});
	});
	
/*	CKEDITOR.replace("contentEditor", {
		
	});*/
});
</script>
<title>Editing Page</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"><?php  include("res/php/sidebar_static.php"); ?></div>
    <div class="page">
    	<form id="pageEdit" method="post" action="ajax/pageUpdateNewsPost.php" onsuccess="showActivityMsg;" >
        <div class="title">
        	<input  name="title" placeholder="Page Title" onkeyup="var tl=this.value.length; if(tl< 5){this.size=5;return;} this.size=tl; "  type="text" value="<?php echo $currentPage->Title; ?>" /><i style="opacity:0;">(editing)</i><span class="btnHolder">
        	<a href="index.php?pid=<?php echo $currentPage->ParentID; ?>" ><img src="res/images/document_shred.png" /></a>
            <a href="#" id="pageSave" onclick="sendAjaxForm('pageEdit')" ><img src="res/images/page_save.png" /></a>
        </span></div>
        
           
            <input style="visibility:hidden; height:0;" type="submit" value="" />
            <div style="position: absolute; top: 100px; bottom: 0;left: 250px; right: 0; display: none;"></div>
            <textarea  style="position: absolute; top: 100px; bottom: 0;left: 250px; right: 0; display: none;"  name="content" class="ckeditor" id="contentEditor" ><?php echo $currentPage->Content; ?></textarea>
            <input name="pageID" type="hidden" value="<?php echo $currentPage->LanguagePageID;  ?>" />
        </form>
        
        
      
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>