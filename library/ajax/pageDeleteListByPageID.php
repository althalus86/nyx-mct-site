<?php 

$toRoot = "../../";
include_once($toRoot.'includes/cms.php');
//include_once('../../includes/SQL.class.php');

$db = new SQL();

$pageIds = explode(',', $_POST['pageIdList']);
$childPages = 0;
foreach ($pageIds as $pageId){
	$childPages += $db->pageDeleteByPageID($pageId, $db->userGetCurrent()->ID);
}

echo count($pageIds)." Pages Deleted";
if ($childPages > 0){
	echo " [with ".$childPages." Child Pages]";
}
?>