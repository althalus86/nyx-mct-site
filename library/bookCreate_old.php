<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
function checkPermalink(){
	$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-").toLowerCase());
		showActivityMsg("Checking permalink validity");
		$.ajax({
		  type: "POST",
		  url: "ajax/pageGetNextPermalink.php",
		  data: "permalink="+$("#permaLink").val()
		}).done(function( msg ) {
			$("#permaLink").val(msg);
		  //alert( "Data Saved: " + msg );
		});
}

$(function(){
	$("#title").keyup(function(){
		$("#permaLink").val($("#title").val().replace(/ /gi, "-").toLowerCase());
	});
	
	$("#title").change(function(){
		checkPermalink();
	});
	
	
	$("#permaLink").keyup(function(){
		$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-").toLowerCase());
	});
	
	$("#permaLink").change(function(){
		checkPermalink();
	});
});
</script>
<title>Create New Book</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title">Create New Page<span class="btnHolder">
        	<a href="index.php?pid=<?php echo $page_parent_id; ?>" ><img src="res/images/document_shred.png" /></a>
        </span></div>
        <form id="pageCreate" method="post" action="pageEdit.php">
            <label>Title:
	            <input type="text" id="title" name="title" />
            </label>
            <label>Perma Link:
	            <input  type="text" id="permaLink" name="permaLink" />
            </label>
            <label>Language:
                <select name="languageID">
                    <option value="1">English</option>
                </select>
            </label>
            <label>Status:
                <select name="state" >
                    <option value="0">Draft</option>
                    <option value="1" selected="selected">Published</option>
                </select>
            </label>
            <label>Type:
                <select name="typeID" >
                	<?php 
						$allPageTypes = $db->pageGetAllTypes();
						foreach ($allPageTypes as $pType){ 
							if ($pType->id > 0){
							echo '<option value="'.$pType->id.'">'.$pType->title.'</option>';
					 } }?>
                </select>
            </label>
            <input type="hidden" name="parentID" value="<?php echo $page_parent_id; ?>" />
            <input type="submit"  class="action_btn" value="Create Page" />
        </form>
      
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>