<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete
/*
	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');*/
//to delete

$db = new SQL();
$currentUser = $db->userGetCurrent();

if(isset($_POST['parentID'])){
	$languagePageID = $db->pageCreate('', $_POST['languageID'], $_POST['title'], $_POST['parentID'], $currentUser->ID, 0, $_POST['state']);
	//echo $languagePageID;
	$linkPageID = $db->pageCreateLink($languagePageID,"",0);
	$currentPage = new PageLink($db->pageGetByLanguageID($languagePageID)->FullPage);
}else if (isset($_GET['lc'])){
	$currentPage = new PageLink($db->pageGetByPageIDandLanguageCode($_GET['pg'], $_GET['lc'])->FullPage);
}


$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script src="res/plugins/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script src="res/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script> 
<script>
	$(function(){
		$("#title").keyup(function(){
			$("#permaLink").val($("#title").val().replace(/ /gi, "-").toLowerCase());
		});
		
		$("#title").change(function(){
			checkPermalink();
		});
		
		
		$("#permaLink").keyup(function(){
			$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-").toLowerCase());
		});
		
		$("#permaLink").change(function(){
			checkPermalink();
		});
		
	});
	
	$(document).ready(function(){
		checkLink();
	});

	function checkPermalink(){
		$("#permaLink").val($("#permaLink").val().replace(/ /gi, "-").toLowerCase());
		
		var permaInternalExists = false;
		if($('#linkI select option[value=<?php echo $currentPage->PermaLink; ?>]').length > 0) { permaInternalExists = true}
		if(($("#permaLink").val() != '<?php echo $currentPage->PermaLink; ?>') || (permaInternalExists)){
			showActivityMsg("Checking permalink validity");
			$.ajax({
			  type: "POST",
			  url: "ajax/pageGetNextPermalink.php",
			  data: "permalink="+$("#permaLink").val()
			}).done(function( msg ) {
				$("#permaLink").val(msg);
			  //alert( "Data Saved: " + msg );
			});
			$('#linkE input').val('http://');
		}
	}

	function checkLink(){
		if($('#linkType').val() == 0){
			$('#linkE').hide();
			$('#permalinkLabel').hide();
			$('#linkI').show();
			$('#linkE input').attr('name','link_not');
			$('#linkI select').attr('name', 'link');
		}else{
			$('#linkI').hide();
			$('#linkE').show();
			$('#permalinkLabel').show();
			$('#linkE input').attr('name','link');
			$('#linkI select').attr('name', 'link_not');
		}
	}
</script>
<title>Editing Page</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
    	<form id="pageEdit" method="post" action="ajax/pageUpdateLink.php" onsuccess="showActivityMsg;" >
        <div class="title">
        	<input id="title"  name="title" placeholder="Page Title" onkeyup="var tl=this.value.length; if(tl< 5){this.size=5;return;} this.size=tl; "  type="text" value="<?php echo $currentPage->Title; ?>" /><i style="opacity:0;">(editing)</i><span class="btnHolder">
        	<a href="index.php?pid=<?php echo $currentPage->ParentID; ?>" ><img src="res/images/document_shred.png" /></a>
            <a href="#" id="pageSave" onclick="sendAjaxForm('pageEdit')" ><img src="res/images/page_save.png" /></a>
        </span></div>
        
           	<div id="pageCreate">
            <label>Type:
                <select id="linkType" name="external" onchange="checkLink()" >
                    <option <?php if($currentPage->External == 0){echo "selected"; } ?> value="0">Internal</option>
                    <option <?php if($currentPage->External == 1){echo "selected"; } ?> value="1">External</option>
                </select>
            </label>
            <label id="linkE">Link External:
	            <input  name="link<?php if($currentPage->External == 0){echo "_not"; } ?>" placeholder="http://" value="<?php echo $currentPage->Link; ?>" />
            </label>
            <label id="linkI">Link Internal:
                <select  name="link<?php if($currentPage->External == 1){echo "_not"; } ?>" >
                	<option disabled="disabled" selected="selected" value='null'>Choose Page by Permalink</option>
                    <option disabled="disabled" value='null'>------------------------</option>
                	<?php 
						$permalinks = $db->pageGetAvailablePermalinks();
						foreach($permalinks as $permalink){
							echo '<option';
							if ($permalink->permalink == $currentPage->PermaLink){echo " selected "; }
							// echo ' value="'.$permalink->permalink.'" >'.generateLevel($permalink->level).$permalink->permalink.'</option>';
							echo ' value="'.$permalink->permalink.'" >'.$permalink->permalink.'</option>';
						}
					
						function generateLevel($level){
							$lead = "";
							if($level > 0){
								$lead .= "&#x2514";
								for ($i = 1; $i <= $level; $i++){
									$lead .= "&#x2500";
								}
								$lead .= "&#x25b6 ";
							}
							
							return $lead;
						}
					?>
                </select>
            </label>
            <label id="permalinkLabel">Perma Link:
	            <input  type="text" id="permaLink" name="permaLink" value="<?php if(strlen($currentPage->PermaLink) > 0){ echo $currentPage->PermaLink; }else{ echo $currentPage->Title;} ?>" />
            </label>
            <input name="pageID" type="hidden" value="<?php echo $currentPage->LanguagePageID;  ?>" />
            </div>
        </form>
        
        
      
    </div>
</div>

<div id="activityBar">loading...</div><script>checkLink(); checkPermalink();</script>
</body>
</html>