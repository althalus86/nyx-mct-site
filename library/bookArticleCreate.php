<?php

$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();


//to delete

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
/*
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);
*/

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>
<?PHP 
$id = 0;
$book = $_GET["book"];
$title = "";
$pages = "";
$authorId = "";
$authorName = "";
$action = "Add New";


if (isset($_GET["id"])) {
	$action = "Edit";
	$row = $db->libraryGetBookArticle($_GET["id"]);
	$id = $_GET["id"];
	$book = $row->book_id;
	$title = $row->title;
	$pages = $row->pages;
	$authorId = $row->author_id;
	$authorName = $row->author_name;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<title><?php echo $action ?> Book Article</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>

<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title"><?php echo $action ?> Book Article<span class="btnHolder">
        	<a href="bookDetails.php?id=<?php echo $book; ?>" ><img src="res/images/document_shred.png" /></a>
        </span></div>
        <form id="pageCreate" method="post" action="bookArticleUpdate.php" onsubmit="return validation_test(this)">
			<input type="hidden" id="id" name="id" value="<?php echo $id ?>" />
            <input type="hidden" id="book" name="book" value="<?php echo $book ?>" />
            <label>Title:
	            <input class="validate" _validatefnc="required" _validateProperty="Title" type="text" id="title" name="title" value="<?php echo $title ?>" />
            </label>
            <label>Pages:
	            <input class="validate" _validatefnc="required" _validateProperty="Pages" type="text" id="pages" name="pages" value="<?php echo $pages ?>" />
            </label>
            <label>Author:
	            <input type="hidden" id="authorId" name="authorId" value="<?php echo $authorId ?>" />
				<input type="text" id="authorName" name="authorName" value="<?php echo $authorName ?>" />
            </label>
            <input type="submit"  class="action_btn" value="<?php echo $action ?> Book Article" />
        </form>
      
    </div>
</div>
<script>
	$(function() {
		$( "#authorName" ).autocomplete({
		  source: "data/authors_search.php",
		  minLength: 2,
		  select: function( event, ui ) {
			$( "#authorId" ).val(ui.item.id);
			$( "#authorName" ).val(ui.item.display);
			return false;
		  }
		});
	});
</script>
<div id="activityBar">loading...</div>
</body>
</html>