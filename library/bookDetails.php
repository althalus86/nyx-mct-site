<?php
$toRoot = "../";
if(isset($_GET['es'])){
	session_start();
	session_destroy();
}
include_once($toRoot.'includes/cms.php');
$Security = new Security();

/*

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
*/

//$statusBar->setStatusID(432);



$languageCode = "en";

$db = new SQL();

$book = $db->libraryGetBookListRow($_GET["id"]);

/*
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

$pagePermissions = $db->permissionsPageGetByPageID($page_parent_id, $db->userGetCurrent()->ID);
*/

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
	$(function() {
		//easyloader.load("datagrid", function(){
			var dg1 = $('#tt').datagrid({
				url:'data/bookArticles_load.php?id=<?php echo $_GET["id"] ?>', 
				onDblClickRow: function(rowIndex, rowData) { 
					window.location = "bookArticleCreate.php?id="+rowData.id;
				},
                remoteFilter: true
            });
            dg1.datagrid('enableFilter');
			
			$('#tt2').datagrid({  
				url:'data/bookLending_load.php?id=<?php echo $_GET["id"] ?>', 
				onDblClickRow: function(rowIndex, rowData) { 
					window.location = "bookLendingCreate.php?id="+rowData.id;
				}
			});
		//});
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( "#dialog-confirm" ).dialog({
			autoOpen: false,
			resizable: false,
			width:400,
			height:220,
			modal: true,
			buttons: {
				"Delete": function() {
					$.ajax({
					  type: "POST",
					  url: "ajax/bookDeleteByBookId.php",
					  data: "id=<?php echo $_GET["id"] ?>"
					}).done(function( msg ) {
						showActivityMsg(msg);
						window.location = "index.php";
					});
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					
					$( this ).dialog( "close" );
				}
			}
		});
		
		
		$( "#sortable" ).sortable({ axis: 'y', opacity: 0.6,  handle: 'span'  });
		$( "#sortable" ).disableSelection();
		$( "#sortable" ).bind( "sortupdate", function(event, ui) {
				var ids = $("#sortable").sortable('serialize');
			  	$.ajax({
				  type: "POST",
				  url: "ajax/saveOrderPageList.php",
				  data: ids
				}).done(function( msg ) {
					showActivityMsg("Sorting has been saved");
				  //alert( "Data Saved: " + msg );
				});
		});
	});
	
	function deleteBook(){
	
		$( '#dialog-confirm' ).dialog( 'open' );
	}
	
	function checkDeleteBtnOpactiy(){
		if($('#sortable input[type=checkbox]:checked').length > 0){
			$('#pagesDeleteBtn').css('opacity','1');
		}else{
			$('#pagesDeleteBtn').css('opacity','0.2');
		}
	}
	
	
</script>
<style>
	#libraryPanels {
		position: absolute;
		left: 300px;
		right: 50px;
	}
	#libraryLeftPanel, #libraryRightPanel {
		position: absolute;
		top: 30px;
		bottom: 0;
	}
	#libraryLeftPanel {
		left: -10px;
		width: 50%;
	}
	#libraryRightPanel {
		right: -5px; 
		width: 50%;
	}
	.libraryAddNew {
		position: absolute;
		right: 2px;
		top: 21px;
	}
	#bookDetails {
		position: relative; 
		margin: 0 40px; 
		padding-top:10px;
	}
	#bookDetails th {
		text-align: left;
		padding-right: 10px;
	}
</style>

<title>Book Details</title>

</head>

<body>
<div id="dialog-confirm" title="Delete Book?">
	<p><span style="float:left; margin:50px 20px 50px 0px;"><img src="res/images/warning.png" /></span>This item will be permanently deleted and cannot be recovered.<p style="text-align:center; font-style:italic; font-weight:bold;">Are you sure?</p></p>
</div>
<?php 
	$sBarUI = new StatusBar();  
	
?>

<div ></div>
<div class="pageContainer">
	<div class="sideBar"><?php $sideBarItem = 0; include("res/php/sidebar.php"); ?></div>
    <div class="page">
        <div class="title">Books<span class="btnHolder">
        	<a href="#" onclick="deleteBook();"><img id="pagesDeleteBtn" src="res/images/page_delete.png" ></a>
        </span></div>
		<div id="bookDetails">
			<h2>Book Details</h2>
			<a class="libraryAddNew" href="bookCreate.php?id=<?php echo $_GET["id"]; ?>">Edit</a>
			   
			<table width="100%">
				<tr>
					<td width="50%">
						<table>
							<tr>
								<th>Book Id:</th>
								<td>#<?php echo $book->id ?></td>
							</tr>
							<tr>
								<th>Title:</th>
								<td><?php echo $book->title ?></td>
							</tr>
							<tr>
								<th>Language:</th>
								<td><?php echo $book->language ?></td>
							</tr>
						</table>
					</td>
					<td width="50%">
						<table>
							<tr>
								<th>Year:</th>
								<td><?php echo $book->year ?></td>
							</tr>
							<tr>
								<th>Volume:</th>
								<td><?php echo $book->volume ?></td>
							</tr>
							<tr>
								<th>Shelf No:</th>
								<td><?php echo $book->shelf ?></td>
							</tr>
							<tr>
								<th>Status:</th>
								<td><?php echo $book->status ?></td>
							</tr>
						</table>				
					</td>
				</tr>
			</table>
		</div>
		<div id="libraryPanels">
		<div id="libraryLeftPanel">
			<h2>Articles</h2>
			<a class="libraryAddNew" href="bookArticleCreate.php?book=<?php echo $_GET["id"]; ?>">Add New</a>
           <table id="tt" class="easyui-datagrid" 
                title="Load Data" iconCls="icon-save"  
                rownumbers="true" pagination="true">  
            <thead>  
                <tr>  
                    <th field="id" width="80" sortable="true" hidden="true">Article ID</th>  
                    <th field="title" width="150" sortable="true">Title</th>  
                    <th field="pages" width="40" sortable="true">Pages</th>  
                    <th field="author_name" width="150" sortable="true">Author</th>  
                </tr>  
            </thead>  
        </table> 
		</div>
		<div id="libraryRightPanel">
		<h2>Lending</h2>
		<a class="libraryAddNew" href="bookLendingCreate.php?book=<?php echo $_GET["id"]; ?>">Add New</a>
		<table id="tt2" class="easyui-datagrid" 
                title="Load Data" iconCls="icon-save"  
                rownumbers="true" pagination="true">  
            <thead>  
                <tr>  
                    <th field="id" width="80" sortable="true" hidden="true">Lending ID</th>  
                    <th field="client_name" width="180" sortable="true">Client Name</th>  
                    <th field="dateTaken" width="80" sortable="true">Date Taken</th>  
                    <th field="dateReturned" width="80" sortable="true">Date Returned</th>  
                </tr>  
            </thead>  
        </table> 
		</div>
		</div>
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>