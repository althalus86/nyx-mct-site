<?php
ini_set('display_errors', '1');
error_reporting(E_ALL);

$toRoot = "../";
include_once($toRoot.'includes/cms.php');
//$Security = new Security();
$db = new SQL();

$dr = 0;
if ($_POST['dateReturned'] > 0) {
	$dr = date("U", strtotime($_POST['dateReturned']));
}

$dt = 0;
if ($_POST['dateTaken'] > 0) {
	$dt = date("U", strtotime($_POST['dateTaken']));
}

$db->libraryAddEditBookLending($_POST['id'], $_POST['clientId'], $_POST['book'], $dt, $dr);

header('Location: bookDetails.php?id='.$_POST['book']);


?>

