<?php
$toRoot = "../";
if(isset($_GET['es'])){
	session_start();
	session_destroy();
}
include_once($toRoot.'includes/cms.php');
$Security = new Security();

/*

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
*/

//$statusBar->setStatusID(432);



$languageCode = "en";

$db = new SQL();
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

$pagePermissions = $db->permissionsPageGetByPageID($page_parent_id, $db->userGetCurrent()->ID);


//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
	var dg1;
	$(function() {
//		easyloader.load("datagrid", function(){
			var dg1 = $('#tt').datagrid({  
				url:'data/books_load.php', 
				onDblClickRow: function(rowIndex, rowData) { 
					window.location = "bookDetails.php?id="+rowData.id;
				},
				remoteFilter: true
			});
			dg1.datagrid('enableFilter');
	//	});
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( "#dialog-confirm" ).dialog({
			autoOpen: false,
			resizable: false,
			width:400,
			height:220,
			modal: true,
			buttons: {
				"Delete all items": function() {
					var pagesToDelete = "";
					$('#sortable input[type=checkbox]:checked').each(function (index){
						pagesToDelete += $(this).parent().parent().attr("id").substr(5)+",";
					});
					pagesToDelete = pagesToDelete.substr(0, pagesToDelete.length -1);
					$.ajax({
					  type: "POST",
					  url: "ajax/pageDeleteListByPageID.php",
					  data: "pageIdList="+pagesToDelete
					}).done(function( msg ) {
						showActivityMsg(msg);
						$('#sortable input[type=checkbox]:checked').each(function (index){
							$(this).parent().parent().remove();
						});
						checkDeleteBtnOpactiy();
					});
					//alert(pagesToDelete);
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					
					$( this ).dialog( "close" );
				}
			}
		});
		
		checkDeleteBtnOpactiy();
		
		$( "#sortable" ).sortable({ axis: 'y', opacity: 0.6,  handle: 'span'  });
		$( "#sortable" ).disableSelection();
		$( "#sortable" ).bind( "sortupdate", function(event, ui) {
				var ids = $("#sortable").sortable('serialize');
			  	$.ajax({
				  type: "POST",
				  url: "ajax/saveOrderPageList.php",
				  data: ids
				}).done(function( msg ) {
					showActivityMsg("Sorting has been saved");
				  //alert( "Data Saved: " + msg );
				});
		});
	});
	
	function deleteSelected(){
		if($('#sortable input[type=checkbox]:checked').length > 0){
			$( '#dialog-confirm' ).dialog( 'open' );
		}
	}
	
	function checkDeleteBtnOpactiy(){
		if($('#sortable input[type=checkbox]:checked').length > 0){
			$('#pagesDeleteBtn').css('opacity','1');
		}else{
			$('#pagesDeleteBtn').css('opacity','0.2');
		}
	}
	
	
</script>
<title>Books</title>

</head>

<body>
<div id="dialog-confirm" title="Delete Selected Pages?">
	<p><span style="float:left; margin:50px 20px 50px 0px;"><img src="res/images/warning.png" /></span>These items will be permanently deleted and cannot be recovered.<br /><br /> All nested children will be deleted as well.<p style="text-align:center; font-style:italic; font-weight:bold;">Are you sure?</p></p>
</div>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"><?php $sideBarItem = 0; include("res/php/sidebar.php"); ?></div>
    <div class="page">
        <div class="title">Books<span class="btnHolder">
        	<a href="bookCreate.php?pid=<?php echo $page_parent_id; ?>" ><img src="res/images/page_add.png" /></a>
        </span></div>
           <table id="tt" class="easyui-datagrid" 
                title="Load Data" iconCls="icon-save"  
                rownumbers="true" pagination="true">  
            <thead>  
                <tr>  
                    <th field="id" width="80" sortable="true">Book ID</th>  
                    <th field="title" width="100" sortable="true">Title</th>  
                    <th field="year" width="80" align="right" sortable="true">Year</th>  
                    <th field="volume" width="80" align="right" sortable="true">Volume</th>  
                    <th field="shelf" width="220">Shelf No</th>
                    <th field="language" width="220">Language</th>
                    <th field="status" width="60" align="center">Status</th>
                </tr>  
            </thead>  
        </table> 
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>