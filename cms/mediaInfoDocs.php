<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

$mediaInfo = $db->mediaGetInfoDocsByID($_GET['mediaID']);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
function saveMediaInfo(){

		$.ajax({
		  type: "POST",
		  url: "ajax/mediaUpdateInfoDocs.php",
		  data: $('#mediaInfo').serialize()
		}).done(function( msg ) {
			showActivityMsg(msg);
		  //alert( "Data Saved: " + msg );
		});
}


</script>
<title>Media Information</title>

</head>

<body>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
<div class="page" style="margin-left:0">
    <form id="mediaInfo" method="post" action="#" onsubmit="return false;">
        <label>Title:
            <input type="text" id="title" name="title" value="<?php echo $mediaInfo->Title; ?>" />
        </label>
        <label>Author:
            <input  type="text" id="author" name="author" value="<?php echo $mediaInfo->Author; ?>" />
        </label>
        <label>Language:
            <select name="languageID">
            		<?php 
						$languages = $db->languagesGetAll();
						
						foreach ($languages as $lang){ 
							$selected = "";
							if($lang->id == $mediaInfo->LanguageID){ $selected = " selected ";}
							echo '<option'.$selected.' value="'.$lang->id.'">'.$lang->title.'</option>';
					 }?>
            </select>
        </label>
        <label>Pub Date:
            <input  type="text" id="dop" name="dop" value="<?php echo $mediaInfo->DateOfPublication; ?>" />
        </label>
        <input type="hidden" name="mediaID" value="<?php echo $_GET['mediaID']; ?>" />
        <button onclick="saveMediaInfo()"  class="action_btn" >Modify Media Info</button>
    </form>
</div></div>

<div id="activityBar">loading...</div>
</body>
</html>