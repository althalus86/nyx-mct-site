<img src="res/images/logo.png" />
<script>
	function showDiag(){
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		
	
		$( "#dialog-media-browser" ).dialog({
			height: ($(document).height() * 0.8),
			width: "80%",
			modal: true,
			
		});
		
	}
	
	function removePageMedia(pageID, mediaID){
		
		$.ajax({
		  type: "POST",
		  url: "ajax/pageDeleteMedia.php",
		  data: "mediaID="+mediaID+"&pageID="+pageID
		}).done(function( msg ) {
			if(msg.length > 0){
				showActivityMsg(msg);
				$('#sideMedia_'+mediaID).remove();
			}
		});
	}
	
	function addPageMedia(mediaID, mediaURL, mediaTitle){
		$(".side_images").append('<li id="sideMedia_'+mediaID+'"><img src="'+mediaURL+'" /><div><span>'+mediaTitle+'</span><span class="sideMedia_remove" onClick="removePageMedia(<?php echo $currentPage->PageID.", "; ?>'+mediaID+')">remove</span></div></li>');
	}
</script>
<div>
	<div id="sideImage_title">Side Images</div>
    <ul class="side_images">
    	<?php 


//$db = new SQL();
		
		$mediaIDList = $db->pageGetMediaIDList($currentPage->PageID);
		
		foreach($mediaIDList as $mediaID ){
			
			$mediaItem = $db->mediaGetByID($mediaID); 
			//echo $mediaItem->getCustomURL("sidethumbs", 45, 0);
			?>
            
            <li id="sideMedia_<?php echo $mediaID; ?>"><img src="<?php  echo $mediaItem->getCustomURL("sidethumbs", 45, 35, "" , "fitin"); ?>" /><div><span><?php  echo $mediaItem->Title; ?></span><span class="sideMedia_remove" onClick="removePageMedia(<?php echo $currentPage->PageID.", ".$mediaID; ?>)">remove</span></div></li>
		<?php }	?>
    	
    </ul>
</div>

<div id="dialog-media-browser" title="Basic modal dialog" style="display:none">
	<iframe width="100%" frameborder="0" src="media_browser.php?only_exts=jpeg,jpg&only_types=1&pageID=<?php if (isset($_GET['pg'])){echo $_GET['pg']; }else{echo 0;}?>"> </iframe>
</div>

<button style="margin:10px;" onClick="showDiag()" >Add Image</button>