// JavaScript Document

function showActivityMsg(msg){
	$("#activityBar").html(msg).animate({
		opacity: 1.0
	}, 500, function(){
		setTimeout(function(){
			$("#activityBar").animate({
				opacity: 0
			},500, function(){
				$("#activityBar").html("");
			});
		}, 1000)
	});
}

function sendAjaxForm(formID){
	
	var form = $('#'+formID);
	form.attr("onsubmit", "return false;");
	
	for (var ckInstance in CKEDITOR.instances){
		CKEDITOR.instances[ckInstance].updateElement();
	}
	
	$.ajax({
		  type: form.attr('method'),
		  url: form.attr('action'),
		  data: form.serialize()
		}).done(function( msg ) {
			eval (form.attr('onsuccess'))(msg);
		});
}