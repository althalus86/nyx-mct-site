<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

//to delete
/*
	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');*/
//to delete

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$db = new SQL();
$currentUser = $db->userGetCurrent();
$media_parent_id = 0;
if (isset($_GET['pid'])){
	$media_parent_id = $_GET['pid'];
}
/*$pagelist = $db->pageGetListAtLevel($media_parent_id);

$page_parent_parent_id = $db->pageGetParentID($media_parent_id);*/

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="res/plugins/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css" rel="stylesheet" type="text/css" />
<?php include_once("res/php/header.php"); ?>

<script type="text/javascript" src="res/plugins/plupload/plupload.full.js"></script>
<script type="text/javascript" src="res/plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<script type="text/javascript">
// Convert divs to queue widgets when the DOM is ready
$(function() {
	
	$(window).resize(function(){
		$("#uploader_filelist").css("height",$('body').innerHeight()-235);
		//CKEDITOR.config.height = $('body').innerHeight()-260;
	});
	
	
	
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5,gears,flash,silverlight,browserplus',
		url : 'upload.php?pid=<?php echo $media_parent_id; ?>',
		max_file_size : '15mb',
		chunk_size : '1mb',
		unique_names : false,

		// Resize images on clientside if we can
		resize : {width : 700, height : 1000, quality : 90},

		// Specify what files to browse for
		filters : [
			{title : "Image files", extensions : "jpg,jpeg"},
			{title : "PDF files", extensions : "pdf,doc"}
		],

		// Flash settings
		flash_swf_url : 'res/plugins/plupload/js/plupload.flash.swf',

		// Silverlight settings
		silverlight_xap_url : 'res/plugins/plupload/js/plupload.silverlight.xap'
	});

	// Client side form validation
	$('form').submit(function(e) {
        var uploader = $('#uploader').pluploadQueue();

        // Files in queue upload them first
        if (uploader.files.length > 0) {
            // When all files are uploaded submit form
            uploader.bind('StateChanged', function() {
                if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                    $('form')[0].submit();
                }
            });
                
            uploader.start();
        } else {
            alert('You must queue at least one file.');
        }

        return false;
    });
	
	$("#uploader_filelist").css("height",$('body').innerHeight()-235);
});
</script>
<title>Create New Page</title>


</head>

<body>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"></div>
    <div class="page">
        <div class="title">Upload Media<span class="btnHolder">
        	<a href="media.php?pid=<?php echo $media_parent_id; ?>" ><img src="res/images/document_shred.png" /></a>
        </span></div>
        <form ..>
            <div id="uploader">
                <p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
            </div>
        </form>
      
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>