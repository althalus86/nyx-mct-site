<?php
$toRoot = "../";
if(isset($_GET['es'])){
	session_start();
	session_destroy();
}
include_once($toRoot.'includes/cms.php');
$Security = new Security();

/*

	include_once('../includes/Users.class.php');
	include_once("../includes/Pages.class.php");
	include_once("../includes/StatusMsg.class.php");
	include_once("../includes/Security.class.php");
	include_once('../includes/SQL.class.php');
*/

//$statusBar->setStatusID(432);



$languageCode = "en";

$db = new SQL();
$page_parent_id = 0;
if (isset($_GET['pid'])){
	$page_parent_id = $_GET['pid'];
}
$pagelist = $db->pageGetListAtLevel($page_parent_id);

$page_parent_parent_id = $db->pageGetParentID($page_parent_id);

$pagePermissions = $db->permissionsPageGetByPageID($page_parent_id, $db->userGetCurrent()->ID);


//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
	$(function() {
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( "#dialog-confirm" ).dialog({
			autoOpen: false,
			resizable: false,
			width:400,
			height:220,
			modal: true,
			buttons: {
				"Delete all items": function() {
					var pagesToDelete = "";
					$('#sortable input[type=checkbox]:checked').each(function (index){
						pagesToDelete += $(this).parent().parent().attr("id").substr(5)+",";
					});
					pagesToDelete = pagesToDelete.substr(0, pagesToDelete.length -1);
					$.ajax({
					  type: "POST",
					  url: "ajax/pageDeleteListByPageID.php",
					  data: "pageIdList="+pagesToDelete
					}).done(function( msg ) {
						showActivityMsg(msg);
						$('#sortable input[type=checkbox]:checked').each(function (index){
							$(this).parent().parent().remove();
						});
						checkDeleteBtnOpactiy();
					});
					//alert(pagesToDelete);
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					
					$( this ).dialog( "close" );
				}
			}
		});
		
		checkDeleteBtnOpactiy();
		
		$( "#sortable" ).sortable({ axis: 'y', opacity: 0.6,  handle: 'span'  });
		$( "#sortable" ).disableSelection();
		$( "#sortable" ).bind( "sortupdate", function(event, ui) {
				var ids = $("#sortable").sortable('serialize');
			  	$.ajax({
				  type: "POST",
				  url: "ajax/saveOrderPageList.php",
				  data: ids
				}).done(function( msg ) {
					showActivityMsg("Sorting has been saved");
				  //alert( "Data Saved: " + msg );
				});
		});
	});
	
	function deleteSelected(){
		if($('#sortable input[type=checkbox]:checked').length > 0){
			$( '#dialog-confirm' ).dialog( 'open' );
		}
	}
	
	function checkDeleteBtnOpactiy(){
		if($('#sortable input[type=checkbox]:checked').length > 0){
			$('#pagesDeleteBtn').css('opacity','1');
		}else{
			$('#pagesDeleteBtn').css('opacity','0.2');
		}
	}
	
	
</script>
<title>Pages</title>

</head>

<body>
<div id="dialog-confirm" title="Delete Selected Pages?">
	<p><span style="float:left; margin:50px 20px 50px 0px;"><img src="res/images/warning.png" /></span>These items will be permanently deleted and cannot be recovered.<br /><br /> All nested children will be deleted as well.<p style="text-align:center; font-style:italic; font-weight:bold;">Are you sure?</p></p>
</div>
<?php $sBarUI = new StatusBar();  ?>
<div ></div>
<div class="pageContainer">
	<div class="sideBar"><?php $sideBarItem = 0; include("res/php/sidebar.php"); ?></div>
    <div class="page">
        <div class="title">Page Viewer<span class="btnHolder">
			<?php if($page_parent_id > 0){ echo '<a href="?pid='.$page_parent_parent_id.'" ><img src="res/images/folder_up.png" /></a>'; } ?>
        	<?php if($pagePermissions->Add <> 0){?><a href="pageCreate.php?pid=<?php echo $page_parent_id; ?>" ><img src="res/images/page_add.png" /></a><?php } ?>
            <?php if($pagePermissions->AddLink <> 0){?><a href="pageCreateLink.php?pid=<?php echo $page_parent_id; ?>" ><img src="res/images/link_add.png" /></a><?php } ?>
            <a href="#" onclick="deleteSelected();" ><img id="pagesDeleteBtn" src="res/images/page_delete.png" /></a>
        </span></div>
        <?php if(count($pagelist) > 0){ ?>
        <ul id="sortable">
            <?php foreach ($pagelist as $bPage){ 
					$bPagePerm = $db->permissionsPageGetByPageID($bPage->PageID, $db->userGetCurrent()->ID); ?>
                    <li class="ui-state-default" id="page_<?php echo $bPage->PageID;  ?>" ><a class="pageTitleBtn" href="?pid=<?php echo $bPage->PageID;  ?>" ><?php if( $pagePermissions->Sortable <> 0){ ?><span></span><?php } if( $bPagePerm->Delete <> 0){?><input type="checkbox" onchange="checkDeleteBtnOpactiy()" /><?php } 
					echo $bPage->Title." <i>(".$bPage->getPageType()->Title.")</i>"; ?></a><?php if( $bPagePerm->Edit <> 0){?><a class="pageEditBtn" href="pageEdit_<?php echo $bPage->getPageType()->InternalName.".php?pg=". $bPage->PageID."&lc=".$languageCode; ?>">edit</a><?php } ?></li>
            <?php }	?>
        </ul>
        <?php }else{ echo '<div class="pageMsg">This page has no children yet.</div>';} ?>
    </div>
</div>

<div id="activityBar">loading...</div>
</body>
</html>