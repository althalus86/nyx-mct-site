<?php

$toRoot = "../";
$isLogin = true;

include_once($toRoot."includes/cms.php");
/*
include_once("../includes/SQL.class.php");
include_once("../includes/StatusMsg.class.php");
include_once("../includes/Security.class.php");*/


$statusBar = new StatusMsg();
if(isset($_GET['e'])){
	$statusBar->setStatusID($_GET['e']);
}
if(!isset($_GET['p'])){
	$_GET['p'] = '/cms/index.php';
}
if(isset($_POST['login'])){
	if (strlen($_POST['loginUsername'])> 0){
		if (strlen($_POST['loginPassword']) > 0){
			
			$db = new SQL();
			$Security = $db->sessionCreate($_POST['loginUsername'], $_POST['loginPassword']);
			if($Security->LoggedIn){
				header( 'Location: '.$_GET['p'] ) ;
				//include("index.php");
				exit(0);
			}else{
				$statusBar = $Security->StatusMsg;
			}
		}
		else{
			$statusBar->setCustomStatus("Password cannot be empty", "error");
		}
	}else{
		$statusBar->setCustomStatus("Username cannot be empty", "error");
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="css/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php if ($statusBar->statusID > -1){ ?>
	<div class="<?php echo $statusBar->statusClass; ?>Bar"><?php if ($statusBar->statusID > 0){echo "#".$statusBar->statusID." - "; } echo $statusBar->statusMsg; ?></div>
<?php } ?>
<div id="loginBox">
	<form method="post">
    	<label for="loginUsername">Username:<br />
 		   	<input id="loginUsername" name="loginUsername"  type="text" class="loginInput" <?php if(($statusBar->statusID != 100) && (isset($_POST['loginUsername']))){ echo 'value="'.$_POST['loginUsername'].'"';} ?> />
        </label>
        <label for="loginPassword">Password:<br />
	        <input id="loginPassword" name="loginPassword" type="password" class="loginInput" />
        </label>
        <input name="login" id="login" value="true" type="hidden" />
        <input class="action_btn" id="loginSubmit" type="submit" value="Login" />
    </form>
</div>

</body>
</html>