<?php 

$toRoot = "../../";
include_once($toRoot.'includes/cms.php');
include_once('../../includes/SQL.class.php');

$db = new SQL();

$pageIds = explode(',', $_POST['pageIdList']);
$childPages = 0;
$path = $db->mediaGetAlbumPath($_POST['mediaID']);
foreach ($pageIds as $mediaID){
	$mediaItem = $db->mediaGetByID($mediaID);
	$mediaPath = "../media/trash/".str_ireplace("\\","/",$mediaItem->Path)."/";
	//if (!file_exists($mediaPath)){@mkdir($mediaPath, 0777, true);}
	if(strlen($mediaItem->Filename) > 0){
		rename("../media/original/".$mediaItem->FullPath, "../media/trash/".$mediaItem->FullPath);
	}else{
		if (!file_exists("../media/original/".str_ireplace("\\","/",$mediaItem->Path))){
			rename("../media/original/".str_ireplace("\\","/",$mediaItem->Path), substr($mediaPath, 0, -1));
		}
	}
	$childPages += $db->mediaDeleteByMediaID($mediaID, $db->userGetCurrent()->ID);
}

echo count($pageIds)." Media Files Deleted";
if ($childPages > 0){
	echo " [with ".$childPages." Child Files]";
}
?>