<?php
$toRoot = "../";
include_once($toRoot.'includes/cms.php');
$Security = new Security();

$only_types = array();
$only_ext = array();

if (isset($_GET['only_types'])){
	if(strlen($_GET['only_types']) > 0){
		$only_types = explode(",",$_GET['only_types']);
	}
}

if (isset($_GET['only_exts'])){
	if(strlen($_GET['only_exts']) > 0){
		$only_ext = explode(",",$_GET['only_exts']);
	}
}

if(!isset($_GET['group'])){
	$_GET['group'] = 0;
}

//$statusBar->setStatusID(432);

if(isset($_GET['es'])){
	session_destroy();
}

$languageCode = "en";

$db = new SQL();
$media_parent_id = 0;
if (isset($_GET['pid'])){
	$media_parent_id = $_GET['pid'];
}
$pageID = $_GET['pageID'];
$medialist = $db->mediaGetListAtLevel($media_parent_id);

$media_parent_parent_id = $db->mediaGetParentID($media_parent_id);

//$statusBar->setCustomStatus("Buzzzzzzz", "error");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("res/php/header.php"); ?>
<script>
function addMedia(mediaID, pageID, mediaTitle){
	$.ajax({
		  type: "POST",
		  url: "ajax/pageAddMedia.php",
		  data: "mediaID="+mediaID+"&pageID="+pageID+"&group=<?php echo $_GET['group']; ?>"
		}).done(function( msg ) {
			if(msg.length > 0){
				parent.addPageMedia(mediaID, msg.substring(3), mediaTitle, <?php echo $_GET['group']; ?>)
				showActivityMsg('Media has been added to page');
			}
		});
}

</script>
<title>Media</title>

</head>

<body>

<div class="page" style="margin-left:0">
    <div class="title">Media<span class="btnHolder">
			<?php if($media_parent_id > 0){ ?> <a href="media_browser.php?only_types=<?php echo implode(',', $only_types); ?>&only_exts=<?php echo implode(',', $only_ext);?>&pid=<?php echo $media_parent_parent_id.'&pageID='.$pageID."&group=".$_GET['group']; ?>" ><img src="res/images/folder_up.png" /></a> <?php } ?>
    </span></div>
    <?php if(count($medialist) > 0){ ?>
    <ol id="selectable">
        <?php foreach ($medialist as $bMedia){ 
		
			$mediaTypeID = $bMedia->getmediaType()->ID;
			if ((in_array($mediaTypeID, $only_types)) || (count($only_types) == 0) || ($mediaTypeID == 0)){
				if ((in_array(substr($bMedia->Extension,1), $only_ext)) || (count($only_ext) == 0) || ($mediaTypeID == 0)){
		
		?>
        		
                <li class="ui-state-default <?php if ($mediaTypeID <> 0) { echo "media"; } else { echo "album"; } ?>" id="media_<?php echo $bMedia->ID;  ?>" >
                    <?php if ($mediaTypeID == 0) { ?>
                                <a  href="media_browser.php?only_types=<?php echo implode(',', $only_types);?>&only_exts=<?php echo implode(',', $only_ext);?>&pid=<?php echo $bMedia->ID."&pageID=".$pageID."&group=".$_GET['group'];  ?>" ><img src="res/images/album.png" /><span class="media_title"><?php echo $bMedia->Title; ?></span></a>
                          <?php } elseif($bMedia->getmediaType()->ID == 1) { ?>
                                <a  href="#" onclick="addMedia(<?php echo $bMedia->ID.", ".$pageID.", '".$bMedia->Title."'";  ?>);" ><img style="margin-top:10px;" src="<?php  echo $bMedia->getThumbURL(); ?>" /><span class="media_title"><?php echo $bMedia->Title; ?></span></a> 
                        <?php  } else { ?>
								<a  href="#" onclick="addMedia(<?php echo $bMedia->ID.", ".$pageID.", '".$bMedia->Title."'";  ?>);" ><img style="margin-top:10px;" width="140" src="<?php   echo $bMedia->getFileTypeIconURL(); ?>" /><span class="media_title"><?php echo $bMedia->Title; ?></span></a> 
						<?php }?>
                </li>
        <?php } }}?>
    </ol>
    <?php }else{ echo '<div class="pageMsg">This folder has no media yet.</div>';} ?>
</div>


<div id="activityBar">loading...</div>
</body>
</html>